﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Repos;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin.Helpers
{
    public class ProductsHelper
    {
        IProductsRepo _repo;
        public ProductsHelper(IProductsRepo repo)
        {
            _repo = repo;
        }

        internal List<ProductFormViewModel> GetProducts(Guid eventId)
        {
            return _repo.GetProducts(eventId);
        }

        internal ProductFormViewModel GetProduct(Guid productId)
        {
            return _repo.GetProduct(productId);
        }

        internal bool AddProduct(ProductFormViewModel model)
        {
            return _repo.AddProduct(model);
        }

        internal bool EditProduct(ProductFormViewModel model)
        {
            return _repo.EditProduct(model);
        }

        internal void DeleteProduct(Guid productId)
        {
            _repo.DeleteProduct(productId);
        }
    }
}
