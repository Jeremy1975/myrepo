﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Models.DiffaEvents;

namespace Diffa.Data.Extensions
{
    public static class DiffaEventTableExtensions
    {
        public static TableFormViewModel ToTableFormViewModel(this DiffaEventTable table)
        {
            return new TableFormViewModel
            {
                TableId = table.TableId,
                TableName = table.TableName,
                SeatsPerTable = table.SeatsPerTable,
                PricePerSeat = table.PricePerSeat,
                HexColor = table.HexColor,
                TablesAvailable = table.TablesAvailable,
                EventId = table.DiffaEvent != null ? table.DiffaEvent.EventId : null
            };
        }

        public static DiffaEventTable ToDiffaEventTable(this TableFormViewModel tableForm)
        {
            return new DiffaEventTable
            {
                TableId = tableForm.TableId,
                TableName = tableForm.TableName,
                SeatsPerTable = tableForm.SeatsPerTable,
                PricePerSeat = tableForm.PricePerSeat,
                HexColor = tableForm.HexColor,
                TablesAvailable = tableForm.TablesAvailable,
                DiffaEventEventId = tableForm.EventId
            };
        }

        public static DiffaEventTable ToEdited(this DiffaEventTable tableForm, TableFormViewModel changes)
        {
            tableForm.TableName = changes.TableName;
            tableForm.SeatsPerTable = changes.SeatsPerTable;
            tableForm.PricePerSeat = changes.PricePerSeat;
            tableForm.HexColor = changes.HexColor;
            tableForm.TablesAvailable = changes.TablesAvailable;
            tableForm.DiffaEventEventId = changes.EventId;
            return tableForm;
        }
    }
}
