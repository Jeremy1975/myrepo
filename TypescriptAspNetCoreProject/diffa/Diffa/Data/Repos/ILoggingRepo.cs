﻿using Diffa.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Repos
{
    public interface ILoggingRepo
    {
        void LogError(ErrorLog log);
        void LogInfo(InformationLog log);
    }
}
