﻿using Diffa.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Models
{
    public class CustomerDetailsViewModel
    {
        public List<ApplicationUser> Users { get; set; }
    }
}
