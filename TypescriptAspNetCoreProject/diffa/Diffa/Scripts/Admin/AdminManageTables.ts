﻿class AdminManageTables {
    promoDateValues: Map<string, Date>
    eventId:string
    constructor() {
        this.promoDateValues = new Map<string, Date>();
    }
    LoadEvents(id:string) {
        $("#liTables").addClass("active");
        $("#TableList").load("/admin/tables?eventId=" + id, () => {
            $("#js-add-table").on("click", (e) => this.AddTable(e));
            $(".js-edit-table").on("click", (e) => this.EditTable(e));
            $(".js-delete-table").on("click", (e) => this.DeleteTable(e));
            $("#js-view-layout").on("click", (e) => {
                this.LoadLayout(e, id);
            })

            this.eventId = id;
        });
    }
    LoadLayout(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>, id: any) {
        $("#table-main-container").hide();
        $("#table-modal-container").load("/admin/tables/table-layout?eventId=" + id, () => {
        });
    }

    AddTable(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#table-main-container").hide();
        $("#table-modal-container").load("/admin/tables/add-table?eventId=" + this.eventId, () => {
            $("#HexColor").kendoColorPicker({
                preview: false,
                value: "#ffffff",
                buttons: false
            });
        });
    }

    EditTable(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#table-main-container").hide();
        $("#table-modal-container").load("/admin/tables/edit-table?tableId=" + $(e.currentTarget).data("id"), () => {
            $("#HexColor").kendoColorPicker({
                preview: false,
                value: "#ffffff",
                buttons: false
            });
        });
    }

    DeleteTable(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        if (confirm("Are you sure you want to delete this table?"))
            window.location.href = "/admin/tables/delete-table?tableId=" + $(e.currentTarget).data("id")
    }
}