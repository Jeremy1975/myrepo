﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace Diffa.Utility
{
    public static class FileUtility
    {
        public static bool SaveFile(Guid eventId, string folder, IFormFile eventLogo, out string path, IWebHostEnvironment environment)
        {
            string wwwPath = environment.WebRootPath;
            path = "";
            string fileName = eventId.ToString();
            switch (eventLogo.ContentType)
            {
                case "image/jpeg":
                    fileName += ".jpg";
                    break;
                case "image/png":
                    fileName += ".png ";
                    break;
                default:
                    return false;
            }

            path = Path.Combine("Uploads", folder, fileName);

            string fullpath = Path.Combine(wwwPath, "Uploads", folder);
            if (!Directory.Exists(fullpath))
            {
                Directory.CreateDirectory(fullpath);
            }

            using (FileStream stream = new FileStream(Path.Combine(fullpath, fileName), FileMode.Create))
            {
                eventLogo.CopyTo(stream);
            }

            return true;
        }
    }
}
