﻿using Diffa.Data.Enums;
using Diffa.Data.Models.DiffaEvents;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Models
{
    public class EventFormViewModel
    {
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public EventType EventType { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string LogoPath { get; set; }
        public int TotalTickets { get; set; }
        public DateTime? BookingStartDate { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public bool AllowInstallmentPlan { get; set; }
        public DateTime? EventStart { get; set; }
        public DateTime? EventEnd { get; set; }
        public IFormFile EventLogo { get; set; }
        public DateTime? PublishedDate { get; set; }
        public IFormFile TableLayout { get; set; }
        public List<DiscountCode> DiscountCodes { get; set; }
    }
}
