﻿class AdminManageUsers {
    
    instance=null;

    constructor() {
        this.instance = this;
    }

    LoadStaffDetails() {
        $("#liUsers").addClass("active");
        $("#StaffDetails").load("/admin/user-detail/staff-details",  ()=> {
            
            $("#js-add-staff").click(e => this.LoadAddStaff(e));
            $(".js-edit-staff").click(e => this.EditStaff(e));
            $(".js-delete-staff").click(e => this.DeleteStaff(e));
            
        });
    }
    DeleteStaff(e: JQuery.ClickEvent<HTMLElement, null, HTMLElement, HTMLElement>): any {
        e.preventDefault();
        if (confirm("Are you sure you want to delete this user")) {


            $.ajax(
                {
                    type: "POST",
                    url: $(e.currentTarget).data("href"),
                    data: "userId=" + $(e.currentTarget).data("user-id"),
                    success: (data) => {
                        if (data.success) {

                            this.LoadStaffDetails();
                        }
                        else {
                            alert(data.message);
                        }
                    },
                    error: function (data) { alert(data); },
                    complete: function () {
                        $("#js-save").attr("disabled", "false");
                    }
                })
        }
    }

    LoadAddStaff(e: JQuery.Event) {
        
        $("#main-container").hide();
        $("#modal-container").load("/admin/user-detail/add-staff", ()=> {
            this.BindStaffForm();
        });
    }

    private EditStaff(e: JQuery.ClickEvent) {
        $("#main-container").hide();

        $("#modal-container").load($(e.currentTarget).data("href"), ()=> {
            this.BindStaffForm();
        });
    }

    private BindStaffForm() {
        
        $.validator.unobtrusive.parse("#js-staff-form");
        $("#js-cancel").on("click", () => { this.CancelModal() });
        $("#js-staff-form").submit(e => this.SubmitForm(e));
        $("#PhoneNumber").mask('000-000-0000');
    }

    private CancelModal () {
        $("#modal-container").html("");
        $("#main-container").show();
    };


    private SubmitForm (e: JQuery.SubmitEvent) {
        e.preventDefault();
        var form = $(e.currentTarget);
        if (!form.valid())
            return;

        $("#js-save").attr("disabled", "true");
        $.ajax(
        {
            type: "POST",
                url: form.data("url"),
            data: form.serialize(),
            success: (data) =>{
                if (data.success) {
                    this.CancelModal();
                    this.LoadStaffDetails();
                }
                else {
                    alert(data.message);
                }
            },
            error: function (data) { alert(data); },
            complete: function () {
                $("#js-save").attr("disabled", "false");
            }
        })
    };

}




