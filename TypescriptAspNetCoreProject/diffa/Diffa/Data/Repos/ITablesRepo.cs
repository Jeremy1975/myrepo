﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Global;
using Diffa.Data.Models.DiffaEvents;
using System;
using System.Collections.Generic;

namespace Diffa.Data.Repos
{
    public interface ITablesRepo
    {
        bool AddEventTable(TableFormViewModel tableForm);
        List<TableFormViewModel> GetEventTables(Guid eventId);
        TableFormViewModel GetEventTable(Guid tableId);
        bool EditEventTable(TableFormViewModel model);
        void DeleteEventTable(Guid tableId);
        List<TablePosition> GetTablePositions(Guid eventId);
        AjaxResult SetLayout(Guid tableId, int tableIndex, double top, double left);
        AjaxResult SetLayout(Guid eventId, string elementName, double top, double left);
        List<EventElementPosition> GetElementPositions(Guid eventId);
    }
}
