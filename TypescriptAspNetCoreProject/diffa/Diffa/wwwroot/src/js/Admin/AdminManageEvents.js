var AdminManageEvents = /** @class */ (function () {
    function AdminManageEvents() {
        this.promoDateValues = new Map();
    }
    AdminManageEvents.prototype.LoadEvents = function () {
        var _this = this;
        $("#liEvents").addClass("active");
        $("#EventList").load("/admin/events/events", function () {
            $("#js-add-event").on("click", function (e) { return _this.LoadAddEvent(e); });
            $(".js-edit-event").on("click", function (e) { return _this.LoadEditEvent(e); });
            $(".js-delete-event").on("click", function (e) { return _this.DeleteEvent(e); });
            $(".js-tables-event").on("click", function (e) { return _this.TablesEvent(e); });
            $(".js-products-event").on("click", function (e) { return _this.ProductsEvent(e); });
            $(".js-publish-event").on("click", function (e) { return _this.PublishEvent(e); });
        });
    };
    AdminManageEvents.prototype.PublishEvent = function (e) {
        if (confirm("Are you sure you want to publish this event?")) {
            window.location.href = "/admin/events/publish-event?eventId=" + $(e.currentTarget).data("id");
        }
    };
    AdminManageEvents.prototype.TablesEvent = function (e) {
        $("#main-container").hide();
        window.location.href = "/admin/events/tables-event?eventId=" + $(e.currentTarget).data("id");
    };
    AdminManageEvents.prototype.ProductsEvent = function (e) {
        $("#main-container").hide();
        window.location.href = "/admin/events/products-event?eventId=" + $(e.currentTarget).data("id");
    };
    AdminManageEvents.prototype.DeleteEvent = function (e) {
        if (confirm("Are you sure you want to delete this event?"))
            window.location.href = "/admin/events/delete-event?eventId=" + $(e.currentTarget).data("id");
    };
    AdminManageEvents.prototype.LoadAddEvent = function (e) {
        var _this = this;
        $("#main-container").hide();
        $("#modal-container").load("/admin/events/add-event", function () {
            _this.BindEventForm();
        });
    };
    AdminManageEvents.prototype.LoadEditEvent = function (e) {
        var _this = this;
        $("#main-container").hide();
        $("#modal-container").load("/admin/events/edit-event?eventId=" + $(e.currentTarget).data("id"), function () {
            _this.BindEventForm();
        });
    };
    AdminManageEvents.prototype.BindEventForm = function () {
        var _this = this;
        $.validator.unobtrusive.parse("#js-event-form");
        $("#js-cancel").on("click", function () { _this.CancelModal(); });
        $("#ZipCode").mask('00000-0000');
        $("#AllowInstallmentPlan").on("click", function () { return $("#divLastPaymentDate").toggle(); });
        $("#btnDiscountCodes").on("click", function (e) { return _this.AddDiscountCodes(e); });
        if ($("#AllowInstallmentPlan:checked").length > 0)
            $("#divLastPaymentDate").toggle();
        $("#js-cancel-promo").on("click", function (e) { return _this.CancelDiscountCodes(e); });
        $("#js-save-promo").on("click", function (e) { return _this.SaveDiscountCodes(e); });
        $("#promoCodeGrid").kendoGrid({
            columns: [
                { field: "name" },
                { field: "price", width: 200 },
                { field: "date", editable: function () { return false; } },
                { field: "flat", editable: function () { return false; }, width: 100 },
                { command: "destroy", width: 200 }
            ],
            editable: {
                mode: "incell"
            },
            remove: function () {
                _this.RemoveRow();
            }
        });
        $("input.js-discount-date").kendoDateTimePicker({
            change: function (e) {
                var existingId = e.sender.element.parents("tr").first().find("input.js-existing-code-hidden").data("existing-id");
                _this.promoDateValues.set(existingId, e.sender.value());
            }
        });
        $("#js-add-promo-code").on("click", function () { return _this.AddPromoRow(); });
        $.validator.addMethod("greaterThanEventStart", function (value, element) {
            var startDate = new Date($("#EventStart").val().toString());
            if (new Date(value) > startDate) {
                return true;
            }
            return false;
        }, "Must be after event start date");
    };
    AdminManageEvents.prototype.SaveDiscountCodes = function (e) {
        e.preventDefault();
        this.CancelDiscountCodes(e);
    };
    AdminManageEvents.prototype.RemoveRow = function () {
        var _this = this;
        window.setTimeout(function () {
            $("input.js-discount-date").each(function (index, element) { return _this.SetDiscountDates(index, element); });
        }, 100);
    };
    AdminManageEvents.prototype.AddPromoRow = function () {
        var _this = this;
        $("#promoCodeGrid").data("kendoGrid").addRow();
        $($("#promoCodeGrid td").get(2)).html("<input class='js-discount-date' />");
        $("input.js-discount-date").each(function (index, element) { return _this.SetDiscountDates(index, element); });
    };
    AdminManageEvents.prototype.SetDiscountDates = function (index, element) {
        var _this = this;
        var id = $(element).parents("tr").first().find("input.js-existing-code-hidden").data("existing-id");
        $(element).kendoDateTimePicker({
            value: this.promoDateValues.get(id),
            change: function (e) {
                var existingId = e.sender.element.parents("tr").first().find("input.js-existing-code-hidden").data("existing-id");
                _this.promoDateValues.set(existingId, e.sender.value());
            }
        });
    };
    AdminManageEvents.prototype.CancelDiscountCodes = function (e) {
        e.preventDefault();
        $("#EventContainer").show();
        $("#PromoContainer").hide();
    };
    AdminManageEvents.prototype.AddDiscountCodes = function (e) {
        e.preventDefault();
        $("#EventContainer").hide();
        $("#PromoContainer").show();
    };
    AdminManageEvents.prototype.CancelModal = function () {
        $("#modal-container").html("");
        $("#main-container").show();
    };
    ;
    return AdminManageEvents;
}());
//# sourceMappingURL=AdminManageEvents.js.map