﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.Cart
{
    public class ShoppingCartDetail
    {
        [Key]
        public Guid ShoppingCartDetailId { get; set; }
        public ShoppingCart ShoppingCart { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
