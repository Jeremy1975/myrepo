﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.Cart
{
    public class ShoppingCartDetailTable
    {
        [Key]
        public Guid ShoppingCartDetailTableId { get; set; }
        public bool IsHost { get; set; }

        public ShoppingCartDetail ShoppingCartDetail { get; set; }
        public int Tickets { get; set; }
    }
}
