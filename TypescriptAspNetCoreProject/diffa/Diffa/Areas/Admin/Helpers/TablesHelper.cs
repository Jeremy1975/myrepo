﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Global;
using Diffa.Data.Models.DiffaEvents;
using Diffa.Data.Repos;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin.Helpers
{
    public class TablesHelper
    {
        ITablesRepo _repo;
        public TablesHelper(ITablesRepo repo)
        {
            _repo = repo;
        }

        public List<TableFormViewModel> GetEventTables(Guid eventId)
        {
            return _repo.GetEventTables(eventId);
        }

        public bool AddEventTable(TableFormViewModel model)
        {
            return _repo.AddEventTable(model);
        }

        public TableFormViewModel GetEventTable(Guid id)
        {
            return _repo.GetEventTable(id);
        }

        public bool EditEventTable(TableFormViewModel model)
        {
            return _repo.EditEventTable(model);
        }

        internal List<TablePosition> GetTablePositions(Guid eventId)
        {
            return _repo.GetTablePositions(eventId);
        }

        internal AjaxResult SetLayout(Guid tableId, int tableIndex, double top, double left)
        {
            return _repo.SetLayout(tableId, tableIndex, top, left);
        }

        internal AjaxResult SetLayout(Guid eventId, string elementName, double top, double left)
        {
            return _repo.SetLayout(eventId, elementName, top, left);
        }

        internal List<EventElementPosition> GetElementPositions(Guid eventId)
        {
            return _repo.GetElementPositions(eventId);
        }
    }
}
