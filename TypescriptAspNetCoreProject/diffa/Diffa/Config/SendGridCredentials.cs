﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Config
{
    public class SendGridCredentials
    {
        public string SendGridUser { get; set; }
        public string SendGridKey { get; set; }
        public string DefaultSender { get; set; }
    }
}
