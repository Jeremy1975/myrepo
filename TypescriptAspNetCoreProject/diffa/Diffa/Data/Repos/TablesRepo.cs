﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Extensions;
using Diffa.Data.Global;
using Diffa.Data.Models.DiffaEvents;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Diffa.Data.Repos
{
    public class TablesRepo : ITablesRepo
    {
        ApplicationDbContext _context;
        public TablesRepo(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool AddEventTable(TableFormViewModel tableForm)
        {
            var newTable = tableForm.ToDiffaEventTable();
            newTable.TableId = Guid.NewGuid();
            newTable.DiffaEvent = _context.DiffaEvents.Where(p => p.EventId == tableForm.EventId).FirstOrDefault();
            _context.Add(newTable);
            _context.SaveChanges();
            return true;
        }

        public void DeleteEventTable(Guid tableId)
        {
            var existing = _context.DiffaEventTable.Where(x => x.TableId == tableId).FirstOrDefault();
            if (existing != null)
            {
                existing.IsDeleted = true;
                _context.Update(existing);
                _context.SaveChanges();
            }
        }

        public bool EditEventTable(TableFormViewModel model)
        {
            var existing = _context.DiffaEventTable.FirstOrDefault(p => p.TableId == model.TableId);
            if (existing == null)
                return false;

            var clonedForSave = existing.ToEdited(model);
            _context.Update(clonedForSave);
            _context.SaveChanges();
            return true;
        }

        public List<EventElementPosition> GetElementPositions(Guid eventId)
        {

            return _context.EventElementPositions.Where(p => p.EventId == eventId).ToList();
        }

        public TableFormViewModel GetEventTable(Guid tableId)
        {
            var record = _context.DiffaEventTable.FirstOrDefault(x => x.TableId == tableId).ToTableFormViewModel();
            return record;
        }

        public List<TableFormViewModel> GetEventTables(Guid eventId)
        {
            List<DiffaEventTable> tables;

            if (eventId == Guid.Empty)
                tables = _context.DiffaEventTable.Where(x => !x.IsDeleted).ToList();
            else
                tables = _context.DiffaEventTable.Where(p => p.DiffaEvent.EventId == eventId && !p.IsDeleted).ToList();

            var eventTables = tables.Select(x => x.ToTableFormViewModel()).ToList();

            return eventTables;
        }

        public List<TablePosition> GetTablePositions(Guid eventId)
        {
            var tables = _context.DiffaEventTable.Where(p=>p.DiffaEvent.EventId==eventId).Select(p => p.TableId).ToList();
            return _context.TablePositions.Where(p => tables.Contains(p.TableId)).ToList();
        }

        public AjaxResult SetLayout(Guid tableId, int tableIndex, double top, double left)
        {
            var position = _context.TablePositions.Where(p => p.TableId == tableId && p.TableIndex == tableIndex).FirstOrDefault();
            if (position == null)
            {
                position = new TablePosition { TableId = tableId, TableIndex = tableIndex, Top = top, Left = left };
                _context.Add(position);
            }
            else
            {
                position.Top = top;
                position.Left = left;
                _context.Update(position);
            }
            this._context.SaveChanges();

            return new AjaxResult{ Success=true};
        }
        public AjaxResult SetLayout(Guid eventId, string elementName, double top, double left)
        {
            var position = _context.EventElementPositions.Where(p => p.EventId == eventId && p.ElementName == elementName).FirstOrDefault();
            if (position == null)
            {
                position = new EventElementPosition { EventId = eventId, ElementName = elementName, Top = top, Left = left };
                _context.Add(position);
            }
            else
            {
                position.Top = top;
                position.Left = left;
                _context.Update(position);
                _context.Update(position);
            }
            this._context.SaveChanges();

            return new AjaxResult { Success = true };
        }
    }
}
