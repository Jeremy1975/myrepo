﻿using Diffa.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Transformed
{
    public class SelectableRoles: ApplicationRole
    {
        public SelectableRoles()
        {

        }
        public SelectableRoles(ApplicationRole role)
        {
            this.Id = role.Id;
            this.Name = role.Name;
        }

        public bool Selected { get; set; }
    }
}
