﻿using Diffa.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Transformed
{
    public class ApplicationUserWithRoles: ApplicationUser
    {
        public ApplicationUserWithRoles(ApplicationUser user)
        {
            this.Id = user.Id;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.PhoneNumber = user.PhoneNumber;
        }

        public List<string> Roles { get; set; }
    }
}
