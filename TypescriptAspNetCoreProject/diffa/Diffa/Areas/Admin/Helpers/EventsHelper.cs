﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Helpers
{
    public class EventsHelper
    {
        IEventsRepo repo;
        public EventsHelper(IEventsRepo repo)
        {
            this.repo = repo;
        }
        public bool AddEvent(EventFormViewModel model)
        {
            return this.repo.AddEvent(model);
        }

        internal List<EventFormViewModel> GetEvents()
        {
            return this.repo.GetEvents();
        }

        internal EventFormViewModel GetEvent(Guid eventId)
        {
            return this.repo.GetEvent(eventId);
        }

        internal bool EditEvent(EventFormViewModel model)
        {
            return this.repo.EditEvent(model);
        }

        internal void DeleteEvent(Guid eventId)
        {
            this.repo.DeleteEvent(eventId);
        }

        internal void PublishEvent(Guid eventId)
        {
            this.repo.PublishEvent(eventId);
        }
    }
}
