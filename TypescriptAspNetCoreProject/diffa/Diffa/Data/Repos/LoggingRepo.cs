﻿using Diffa.Data.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Repos
{
    public class LoggingRepo: ILoggingRepo
    {
        private ApplicationDbContext applicationDbContext;
        IHttpContextAccessor httpContextAccessor;
        public LoggingRepo(ApplicationDbContext applicationDbContext, IHttpContextAccessor httpContextAccessor)
        {
            this.applicationDbContext = applicationDbContext;
            this.httpContextAccessor = httpContextAccessor;
        }

        public void LogError(ErrorLog log)
        {
            log.Id = Guid.NewGuid();
            log.User = GetUser();
            applicationDbContext.Add(log);
            applicationDbContext.SaveChanges();

        }

        private string GetUser()
        {
            try
            {
                var user = httpContextAccessor.HttpContext.User;
                if (user != null)
                    return user.Identity.Name;
            }

            catch{}
            return String.Empty;
        }

        public void LogInfo(InformationLog log)
        {
            try
            {
                log.Id = Guid.NewGuid();
                log.User = GetUser();
                applicationDbContext.Add(log);
                applicationDbContext.SaveChanges();
            }
            catch { }
        }
    }
}
