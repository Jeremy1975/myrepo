<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
	<title></title>
	<meta name="generator" content="https://conversiontools.io" />
	<meta name="author" content="Alexis dS"/>
	<meta name="created" content="2022-07-16T18:02:34"/>
	<meta name="changedby" content="Alexis dS"/>
	<meta name="changed" content="2022-07-18T15:41:50"/>
	<meta name="AppVersion" content="16.0300"/>
	<meta name="DocSecurity" content="0"/>
	<meta name="HyperlinksChanged" content="false"/>
	<meta name="LinksUpToDate" content="false"/>
	<meta name="ScaleCrop" content="false"/>
	<meta name="ShareDoc" content="false"/>

	<style type="text/css">
		body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small }
		a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  }
		a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  }
		comment { display:none;  }
	</style>
</head>
<body>
<table cellspacing="0" border="0">
	<colgroup span="8" width="89"></colgroup>
	<tr>
		<td style="border-top: 2px solid #000000; border-left: 2px solid #000000" rowspan=3 height="78" align="left" valign=bottom><b><font color="#000000"><br><img src="{{asset('images/logo.png')}}" width=71 height=72 hspace=9 vspace=4>
		</font></b></td>
		<td style="border-top: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 2px solid #000000; border-right: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-right: 2px solid #000000" colspan=7 align="left" valign=bottom><b><font size=6 color="#000000">LOAD CONFIRMATION</font></b></td>
    </tr>
	<tr>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td colspan=2 align="right" valign=bottom><b><font color="#000000">ORDER NUMBER: </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" colspan=2 align="left" valign=bottom>
            <b><font size=4 color="#000000"><br></font></b>{{$order->branch}}&nbsp;&nbsp;&nbsp;{{$order->id}}
        </td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000" colspan=2 height="29" align="left" valign=bottom><b><font face="Copperplate Gothic Bold" size=5 color="#000000">UNIDEL </font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td colspan=2 align="right" valign=bottom><b><font color="#000000">DATE: </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" colspan=2 align="center" valign=bottom>
            <b><font color="#000000"><br></font></b>{{$order->date_created}}
        </td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000" colspan=3 height="19" align="left" valign=bottom><b><font face="Avenir Next LT Pro Demi" color="#767171">In Africa for the long haul</font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-right: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-left: 2px solid #000000" height="8" align="left" valign=bottom><b><font face="Avenir Next LT Pro Demi" color="#767171"><br></font></b></td>
		<td align="left" valign=bottom><b><font face="Avenir Next LT Pro Demi" color="#767171"><br></font></b></td>
		<td align="left" valign=bottom><b><font face="Avenir Next LT Pro Demi" color="#767171"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-right: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-left: 2px solid #000000" height="7" align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		<td style="border-right: 2px solid #000000" align="left" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="40" align="left" valign=bottom>
            <b><font color="#000000">TO: </font></b><u>{{$transporter->name}}</u>
        </td>
    </tr>
	<tr>
		<td style="border-left: 2px solid #000000" colspan=4 height="40" align="left" valign=bottom>
            <b><font color="#000000">ATTENTION: </font></b><u>{{$transporter_order_detail->gi_att}}</u>
        </td>
		<td style="border-right: 2px solid #000000" colspan=4 align="left" valign=bottom>
            <b><font color="#000000">FROM: </font></b><u>{{$user->name}}</u>
        </td>
    </tr>
	<tr>
		<td style="border-left: 2px solid #000000" height="16" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-right: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="6" align="center" valign=bottom bgcolor="#7F7F7F"><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 height="38" align="left" valign=bottom>
            <b><font color="#000000">1. LOAD AT:</font></b>&nbsp;&nbsp;&nbsp;{{!empty($loadAddr) ? $loadAddr->name : ''}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=bottom>
            <b><font color="#000000">1. OFFLOAD AT:</font></b>&nbsp;&nbsp;&nbsp;{{!empty($offloadAddr) ? $offloadAddr->name : ''}}
        </td>
    </tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 height="38" align="left" valign=bottom>
            &nbsp;{{!empty($loadAddr) ? $loadAddr->address1 : ''}}&nbsp;&nbsp;&nbsp;{{!empty($loadAddr) ? $loadAddr->address2 : ''}}&nbsp;&nbsp;&nbsp;{{!empty($loadAddr) ? $loadAddr->address3 : ''}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=bottom>
            &nbsp;{{!empty($offloadAddr) ? $offloadAddr->address1 : ''}}&nbsp;&nbsp;&nbsp;{{!empty($offloadAddr) ? $offloadAddr->address2 : ''}}&nbsp;&nbsp;&nbsp;{{!empty($offloadAddr) ? $offloadAddr->address3 : ''}}
        </td>
    </tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 height="38" align="left" valign=bottom>
            &nbsp;{{!empty($loadAddr) ? $loadAddr->city : ''}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=bottom>
            &nbsp;{{!empty($offloadAddr) ? $offloadAddr->city : ''}}
        </td>
    </tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 height="38" align="left" valign=bottom>
            &nbsp;{{!empty($loadAddr) ? $loadAddr->province1 : ''}}&nbsp;&nbsp;&nbsp;{{!empty($loadAddr) ? $loadAddr->province2 : ''}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=bottom>
            &nbsp;{{!empty($offloadAddr) ? $offloadAddr->province1 : ''}}&nbsp;&nbsp;&nbsp;{{!empty($offloadAddr) ? $offloadAddr->province2 : ''}}
        </td>
    </tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 height="33" align="left" valign=bottom>
            <b><font color="#000000">LOAD DATE:</font></b>&nbsp;&nbsp;&nbsp;{{!empty($loadAddr) ? $loadAddr->created_at : ''}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=bottom>
            <b><font color="#000000">OFFLOAD DATE:</font></b>&nbsp;&nbsp;&nbsp;{{!empty($offloadAddr) ? $offloadAddr->created_at : ''}}
        </td>
    </tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 height="33" align="left" valign=bottom><b><font color="#000000">CONTACT:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=bottom><b><font color="#000000">TEL:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=bottom><b><font color="#000000">CONTACT:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=bottom><b><font color="#000000">TEL:</font></b></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="6" align="center" valign=bottom bgcolor="#7F7F7F"><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000" colspan=3 height="36" align="left" valign=bottom>
            <b><font color="#000000">COMMODITY: </font></b>{{$transporter_order_detail->gi_desc}}
        </td>
		<td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=bottom>
            <b><font color="#000000">WEIGHT: </font></b>{{$transporter_order_detail->gi_tons}}
        </td>
		<td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" colspan=3 align="left" valign=bottom>
            <b><font color="#000000">NOTE: </font></b>{{$transporter_order_detail->gi_instruction}}
        </td>
    </tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="36" align="left" valign=bottom>
            <b><font color="#000000">DIMENSIONS:</font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000">L - </font></b>{{$transporter_order_detail->v_l}} m
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000">W - </font></b>{{$transporter_order_detail->v_w}} m
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000">H - </font></b>{{$transporter_order_detail->v_h}} m
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000">EQUIPMENT REQUIRED: </font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000"><br></font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000"><br></font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000"><br></font></b>
        </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 height="38" align="left" valign=bottom>
            <b><font color="#000000">LOAD TYPE: </font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=bottom>
            <b><font color="#000000">VEHICLE REQUIRED: </font></b>{{$vehicle_type->name}}
        </td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000" colspan=3 height="38" align="left" valign=bottom bgcolor="#7F7F7F"><b><font color="#FFFFFF">ADDITIONAL INSTRUCTION:</font></b></td>
		<td align="left" valign=bottom bgcolor="#7F7F7F"><b><font color="#FFFFFF"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#7F7F7F"><b><font color="#FFFFFF"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#7F7F7F"><b><font color="#FFFFFF"><br></font></b></td>
		<td align="left" valign=bottom bgcolor="#7F7F7F"><b><font color="#FFFFFF"><br></font></b></td>
		<td style="border-right: 2px solid #000000" align="left" valign=bottom bgcolor="#7F7F7F"><b><font color="#FFFFFF"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000" colspan=2 height="38" align="left" valign=bottom><b><font color="#000000">DRIVER:</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><b><font color="#000000">CONTAINER TYPE</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><b><font color="#000000">CONTAINER NUMBER</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><b><font color="#000000">VESSEL:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" colspan=2 align="center" valign=bottom><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000" colspan=2 height="35" align="left" valign=bottom><b><font color="#000000">TRUCK                 REGISTRATION:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=bottom><b><font color="#000000">TRAILER REGISTRATION:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" colspan=2 align="center" valign=bottom><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000" colspan=2 height="42" align="left" valign=bottom>
            <b><font color="#000000">RATE (Excl. V.A.T.): </font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000"> </font></b>{{$transporter_order_detail->gi_rate}}&nbsp;{{$transporter_order_detail->gi_currency}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000">TERMS OF PAYMENT: </font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom>
            <b><font color="#000000"> </font></b>{{$transporter_order_detail->gi_terms}}
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=bottom>
            <b><font color="#000000">GOODS IN TRANSIT </font></b>
        </td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" align="left" valign=bottom>
            <b><font color="#000000"><br></font></b>{{$transporter_order_detail->gi_value}}&nbsp;{{$transporter_order_detail->gi_currency}}
        </td>
	</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="6" align="center" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 2px solid #000000; border-right: 1px solid #000000" colspan=2 height="19" align="left" valign=bottom><b><font color="#000000">EMERGENCY NUMBERS:</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=bottom><b><font color="#000000">RICK: 082 492 7691</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=bottom><b><font color="#000000">KOOS: 083 298 9188 </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 2px solid #000000" colspan=2 align="center" valign=bottom><b><font color="#000000">GENO: 083 452 0872</font></b></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="6" align="center" valign=bottom bgcolor="#7F7F7F"><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000" height="19" align="left" valign=bottom><b><font color="#000000">NOTES</font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
		<td style="border-right: 2px solid #000000" align="left" valign=bottom><b><font color="#000000"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="19" align="left" valign=bottom><font color="#000000">All invoices must reflect the UC Order number and must be submitted with clients original delivery note.</font></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="19" align="left" valign=bottom><font color="#000000">Payment: Invoice and Original Proof of Delivery submitted before cut off date 25th of the month.</font></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="20" align="left" valign=bottom><font color="#000000">Unidel Carriers (Pty) Ltd will not be liable for payments due on invoices received later than 6 months from delivery of this load.</font></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="59" align="left" valign=bottom><font color="#000000">The transporter/subcontractor agrees that the value of the Goods in Transit insurance requested on this order, will be legally binding and, by carrying out this instruction, hereby constitutes acceptance and indemnifies Unidel Carriers (Pty) Ltd from liability to the value  requested on this order.</font></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="42" align="left" valign=bottom><font color="#000000">The transporter/subcontractor will not subcontract and/or tranship this load without prior consent from Unidel Carriers (Pty) Ltd.</font></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="6" align="center" valign=bottom bgcolor="#000000"><b><font color="#000000"><br></font></b></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="16" align="center" valign=bottom><font size=1 color="#000000">Unidel Carriers (Pty) Ltd: Reg. No: 1995/00090/07 P O Box 3282 Benoni 1500 Gauteng South Africa www.unidel.co.za info@unidel.co.za</font></td>
		</tr>
	<tr>
		<td style="border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="16" align="center" valign=bottom><font size=1 color="#000000">Johannesburg: 285/2 Deodar Road, Pomona, Kempton Park Tel: +27 (0)11 565 9000 </font></td>
		</tr>
	<tr>
		<td style="border-bottom: 2px solid #000000; border-left: 2px solid #000000; border-right: 2px solid #000000" colspan=8 height="16" align="center" valign=bottom><font size=1 color="#000000">Directors: R. de Smidt (Managing) M.A. de Smidt (Financial)</font></td>
		</tr>
</table>
</body>
</html>
