﻿using Diffa.Data;
using Diffa.Data.Models;
using Diffa.Policies;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;

namespace Diffa.Seeding
{
    public static class SeedDataInitializer
    {
        public static void SeedData(UserManager<ApplicationUser> userManager,RoleManager<ApplicationRole> roleManager, ApplicationDbContext applicationDbContext)
        {
            SeedDataInitializer.SeedRoles(roleManager);
            SeedDataInitializer.SeedUsers(userManager);
            SeedDataInitializer.SeedModules(applicationDbContext);
            SeedDataInitializer.SeedRoleModules(roleManager,applicationDbContext);
        }

        private static void SeedRoleModules(RoleManager<ApplicationRole> roleManager,ApplicationDbContext applicationDbContext)
        {
            var role = roleManager.Roles.Where(p => p.Name == "Administrator").FirstOrDefault();
            if (role != null)
            {
                foreach(var applicationModule in applicationDbContext.ApplicationModules)
                {
                    if(!applicationDbContext.RoleModules.Where(p=>p.RoleId== role.Id && p.ModuleId==applicationModule.Id).Any())
                    {
                        applicationDbContext.Add(new RoleModule
                        {
                             Id=Guid.NewGuid(),
                             ModuleId=applicationModule.Id,
                             RoleId= role.Id
                        });
                    }
                }
                applicationDbContext.SaveChanges();
            }
        }

        private static void SeedModules(ApplicationDbContext applicationDbContext)
        {
            var modules = PolicyNames.GetAllModuleNames();
            foreach(var module in modules)
            {
                if (!applicationDbContext.ApplicationModules.Where(p => p.ModuleName == module).Any())
                {
                    applicationDbContext.Add(new ApplicationModule
                    {
                        Id = Guid.NewGuid(),
                        ModuleName = module
                    });
                }
            }

            applicationDbContext.SaveChanges();
        }

        private static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByNameAsync("kt-admin@kitelytech.com").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "kt-admin@kitelytech.com";
                user.Email = "kt-admin@kitelytech.com";
                user.EmailConfirmed=true;
                user.IsStaff = true;
                IdentityResult result = userManager.CreateAsync(user, "Password123##").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Administrator").Wait();
                }
            }


        }

        private static void SeedRoles(RoleManager<ApplicationRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("User").Result)
            {
                ApplicationRole role = new ApplicationRole();
                role.Name = "User";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }


            if (!roleManager.RoleExistsAsync("Administrator").Result)
            {
                ApplicationRole role = new ApplicationRole();
                role.Name = "Administrator";
                role.IsStaff = true;
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
        }
    }
}
