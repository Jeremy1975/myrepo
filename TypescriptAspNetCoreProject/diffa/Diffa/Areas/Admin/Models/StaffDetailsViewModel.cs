﻿using Diffa.Data.Models;
using Diffa.Data.Transformed;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Models
{
    public class StaffDetailsViewModel
    {
        public List<ApplicationUserWithRoles> Users { get; set; }
        public List<ApplicationRole> Roles { get; set; }
    }
}
