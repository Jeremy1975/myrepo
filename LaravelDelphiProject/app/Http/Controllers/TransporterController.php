<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use \PDF;
use Exception;
use App\Models\Transporter;
use App\Models\TransporterOrder;
use App\Models\Order;
use App\Models\TransporterOrderDetail;
use App\Models\VehicleType;
use App\Models\LoadAddress;
use App\Models\OffLoadAddress;
use Auth;


class TransporterController extends Controller
{
    public function getTransporterList()
    {
        $transporters = Transporter::select('*')->get();
        return view('transporter/list', ['transporters' => $transporters]);
    }

    public function getAllTransporters()
    {
        $transporters = Transporter::select('*')->get();
        return response()->json(['success' => $transporters]);
    }

    public function add(Request $request)
    {
        $order_id = $request->order_id;
        $add_transporter_id = $request->add_transporter_id;

        $result = 0;
        try{
            $existing_record = TransporterOrder::select('*')->where('order_id', $order_id)->where('transporter_id', $add_transporter_id)->first();
            if(empty($existing_record)){
                $new_transporter_order = TransporterOrder::create(['order_id' => $order_id, 'transporter_id' => $add_transporter_id]);
                if($new_transporter_order){
                    $result = $new_transporter_order->id;
                }
            }
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function create(Request $request)
    {
        $account_no = $request->account_no;
        $transporter_name = $request->transporter_name;
        $transporter_address1 = $request->transporter_address1;

        $result = 0;
        try{
            $new_transporter = Transporter::create(
            [
                'account_no' => $account_no,
                'name' => $transporter_name,
                'address1' => $transporter_address1
            ]);

            if($new_transporter){
                $result = 1;
            }
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function edit(Request $request)
    {
        $edit_transporter_id = $request->edit_transporter_id;
        $edit_account_no = $request->edit_account_no;
        $edit_transporter_name = $request->edit_transporter_name;
        $edit_transporter_address1 = $request->edit_transporter_address1;

        $result = 0;
        try{
            Transporter::where('id', $edit_transporter_id)->update(
            [
                'account_no' => $edit_account_no,
                'name' => $edit_transporter_name,
                'address1' => $edit_transporter_address1
            ]);

            $result = 1;
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function delete(Request $request)
    {
        $order_id = $request->order_id;
        $delete_transporter_id = $request->delete_transporter_id;

        $result = 0;
        try{
            $delete_order_transporter = TransporterOrder::where('order_id', $order_id)->where('transporter_id', $delete_transporter_id);

            if($delete_order_transporter){
                $delete_order_transporter->delete();
                $result = 1;
            }
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function remove(Request $request)
    {
        $remove_transporter_id = $request->remove_transporter_id;

        $result = 0;
        try{
            $remove_order_transporters = TransporterOrder::where('transporter_id', $remove_transporter_id);

            if($remove_order_transporters){
                $remove_order_transporters->delete();
            }

            $remove_transporter = Transporter::where('id', $remove_transporter_id);
            if($remove_transporter){
                $remove_transporter->delete();
                $result = 1;
            }
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function change(Request $request)
    {
        $order_id = $request->order_id;
        $change_transporter_id = $request->change_transporter_id;

        $result = 0;
        try{
            TransporterOrder::where('order_id', $order_id)->update(
            [
                'transporter_id' => $change_transporter_id
            ]);

            $result = 1;
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function getOrderTransporter($id)
    {
        $record = TransporterOrder::select('*')->where('id', $id)->first();
        $order = Order::select('*')->where('id', $record->order_id)->first();
        $transporter = Transporter::select('*')->where('id', $record->transporter_id)->first();
        $transporter_order_detail = TransporterOrderDetail::select('*')->where('transporter_order_id', $record->id)->first();

        $loadAddrs = LoadAddress::select('*')->get();
        $offLoadAddrs = OffLoadAddress::select('*')->get();

        $select_loadAddr = null;
        if($transporter_order_detail && $transporter_order_detail->load_addr_id){
            $select_loadAddr = $loadAddrs->where('id', $transporter_order_detail->load_addr_id)->first();
        }

        $select_offloadAddr = null;
        if($transporter_order_detail && $transporter_order_detail->offload_addr_id){
            $select_offloadAddr = $offLoadAddrs->where('id', $transporter_order_detail->offload_addr_id)->first();
        }

        return view('transporter/detail',
            ['id' => $id, 'order' => $order, 'transporter' => $transporter,
            'transporter_order_detail' => $transporter_order_detail, 'loadAddrs' => $loadAddrs,
            'offLoadAddrs' => $offLoadAddrs, 'select_loadAddr' => $select_loadAddr, 'select_offloadAddr' => $select_offloadAddr]);
    }

    public function saveConfirmationData(Request $request)
    {
        $transporter_order_id = $request->info[0];
        $gi_att = $request->info[1];
        $gi_desc = $request->info[2];
        $gi_tons = $request->info[3];
        $gi_abnormal = $request->info[4];
        $gi_instruction = $request->info[5];
        $gi_reqd = $request->info[6];
        $gi_value = $request->info[7];
        $gi_currency = $request->info[8];
        $gi_rate = $request->info[9];
        $gi_terms = $request->info[10];
        $v_type_id = $request->info[11];
        $v_l = $request->info[12];
        $v_w = $request->info[13];
        $v_h = $request->info[14];
        $v_add_dimension = $request->info[15];
        $load_addr_id = $request->info[16];
        $offload_addr_id = $request->info[17];

        $result = 0;
        try{
            $record = TransporterOrderDetail::select('*')->where('transporter_order_id', $transporter_order_id)->first();
            if(!empty($record)){
                TransporterOrderDetail::where('transporter_order_id', $transporter_order_id)->update(
                [
                    'gi_att' => $gi_att,
                    'gi_desc' => $gi_desc,
                    'gi_tons' => $gi_tons,
                    'gi_abnormal' => $gi_abnormal,
                    'gi_reqd' => $gi_reqd,
                    'gi_value' => $gi_value,
                    'gi_rate' => $gi_rate,
                    'gi_currency' => $gi_currency,
                    'gi_terms' => $gi_terms,
                    'gi_instruction' => $gi_instruction,
                    'v_type_id' => $v_type_id,
                    'v_l' => $v_l,
                    'v_w' => $v_w,
                    'v_h' => $v_h,
                    'v_add_dimension' => $v_add_dimension,
                    'load_addr_id' => $load_addr_id,
                    'offload_addr_id' => $offload_addr_id
                ]);
            }
            else{
                TransporterOrderDetail::create(
                [
                    'transporter_order_id' => $transporter_order_id,
                    'gi_att' => $gi_att,
                    'gi_desc' => $gi_desc,
                    'gi_tons' => $gi_tons,
                    'gi_abnormal' => $gi_abnormal,
                    'gi_reqd' => $gi_reqd,
                    'gi_value' => $gi_value,
                    'gi_currency' => $gi_currency,
                    'gi_rate' => $gi_rate,
                    'gi_terms' => $gi_terms,
                    'gi_instruction' => $gi_instruction,
                    'v_type_id' => $v_type_id,
                    'v_l' => $v_l,
                    'v_w' => $v_w,
                    'v_h' => $v_h,
                    'v_add_dimension' => $v_add_dimension,
                    'load_addr_id' => $load_addr_id,
                    'offload_addr_id' => $offload_addr_id
                ]);
            }

            $result = $transporter_order_id;
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function generatePDF($id)
    {
        $record = TransporterOrder::select('*')->where('id', $id)->first();
        $order = Order::select('*')->where('id', $record->order_id)->first();
        $transporter = Transporter::select('*')->where('id', $record->transporter_id)->first();
        $user = Auth::user();
        $transporter_order_detail = TransporterOrderDetail::select('*')->where('transporter_order_id', $id)->first();
        $vehicle_type = VehicleType::select('*')->where('id', $transporter_order_detail->v_type_id)->first();
        $loadAddr = LoadAddress::select('*')->where('id', $transporter_order_detail->load_addr_id)->first();
        $offloadAddr = OffLoadAddress::select('*')->where('id', $transporter_order_detail->offload_addr_id)->first();

        $pdf = PDF::loadview('pdf/order', compact('order', 'transporter', 'user', 'transporter_order_detail', 'vehicle_type', 'loadAddr', 'offloadAddr'));
        return $pdf->stream();
    }
}
