﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin.Models
{
    public class ProductFormViewModel
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string LogoPath { get; set; }
        public IFormFile ProductLogo { get; set; }
        public Guid? EventId { get; set; }
        public List<SelectListItem> Events { get; set; } = new List<SelectListItem>();
    }
}
