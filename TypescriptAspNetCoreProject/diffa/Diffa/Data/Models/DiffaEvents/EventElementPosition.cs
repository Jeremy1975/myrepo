﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.DiffaEvents
{
    public class EventElementPosition
    {
        public Guid EventId { get; set; }
        public string ElementName { get; set; }
        public double Top { get; set; }
        public double Left { get; set; }
    }
}
