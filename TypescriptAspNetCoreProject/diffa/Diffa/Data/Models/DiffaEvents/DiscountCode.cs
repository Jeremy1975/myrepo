﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.DiffaEvents
{
    public class DiscountCode
    {
        [Key]
        public Guid DiscountCodeId { get; set; }

        public string Code { get; set; }
        public DiffaEvent DiffaEvent { get; set; }
        public bool IsFlatDiscount { get; set; }
        public double PriceOrPercentage { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int Quantity { get; set; }
    }
}
