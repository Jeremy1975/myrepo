﻿using Diffa.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Home.Models
{
    public class HomeModel
    {
        public ApplicationUser CurrentUser { get; set; }
    }
}
