﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Extensions;
using Diffa.Data.Models.DiffaEvents;
using Diffa.Utility;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Diffa.Data.Repos
{
    public class ProductsRepo : IProductsRepo
    {
        ApplicationDbContext _context;
        private IWebHostEnvironment _environment;
        public ProductsRepo(ApplicationDbContext context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public bool AddProduct(ProductFormViewModel productForm)
        {
            var newProduct = productForm.ToDiffaProduct();
            newProduct.ProductId = Guid.NewGuid();
            if(productForm.ProductLogo != null)
            {
                string path = string.Empty;
                if (FileUtility.SaveFile(newProduct.ProductId, "Logo", productForm.ProductLogo, out path, _environment))
                    newProduct.LogoPath = path;
            }

            _context.Add(newProduct);
            _context.SaveChanges();
            return true;
        }

        public void DeleteProduct(Guid productId)
        {
            var existing = _context.DiffaProducts.FirstOrDefault(p => p.ProductId == productId);
            if (existing != null)
            {
                existing.IsDeleted = true;
                _context.Update(existing);
                _context.SaveChanges();
            }
        }

        public bool EditProduct(ProductFormViewModel model)
        {
            var existing = _context.DiffaProducts.Where(p => p.ProductId == model.ProductId).FirstOrDefault();
            if (existing == null)
                return false;

            var clonedForSave = existing.ToEdited(model);
            if (model.ProductLogo != null)
            {
                string path = string.Empty;
                if (FileUtility.SaveFile(model.ProductId, "Logo", model.ProductLogo, out path, _environment))
                    clonedForSave.LogoPath = path;
            }

            _context.Update(clonedForSave);
            _context.SaveChanges();
            return true;
        }

        public ProductFormViewModel GetProduct(Guid productId)
        {
            return GetLiveProducts().FirstOrDefault(p => p.ProductId == productId)?.ToProductFormViewModel();
        }

        public List<ProductFormViewModel> GetProducts(Guid eventId)
        {
            List<DiffaProduct> products;

            if(eventId == Guid.Empty)
                products = GetLiveProducts().Where(x => !x.IsDeleted).ToList();
            else
                products = GetLiveProducts().Where(x => x.DiffaEvent.EventId == eventId && !x.IsDeleted).ToList();

            var eventProducts = products.Select(p => p.ToProductFormViewModel()).ToList();

            return eventProducts;
        }

        private IQueryable<DiffaProduct> GetLiveProducts()
        {
            return _context.DiffaProducts.Where(p => !p.IsDeleted);
        }
    }
}
