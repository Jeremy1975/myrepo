﻿using Diffa.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Repos
{
    public interface IEventsRepo
    {
        bool AddEvent(EventFormViewModel eventForm);
        List<EventFormViewModel> GetEvents();
        EventFormViewModel GetEvent(Guid eventId);
        bool EditEvent(EventFormViewModel model);
        void DeleteEvent(Guid eventId);
        void PublishEvent(Guid eventId);
    }
}
