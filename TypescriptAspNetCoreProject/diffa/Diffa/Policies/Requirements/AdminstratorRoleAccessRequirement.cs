﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace Diffa.Policies.Requirements
{
    public class AdminstratorRoleAccessRequirement : IAuthorizationRequirement
    {
        public string ModuleName { get; }

        public AdminstratorRoleAccessRequirement(string moduleName)
        {
            ModuleName = moduleName;
        }
    }
}
