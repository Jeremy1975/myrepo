@push('styles')
    <link href="{{asset('plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/select2/css/select2.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/google-code-prettify/prettify.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@extends('layouts.master')
@section('title', 'Tranporter')
@section('content')
<div class="row">
    <div class="col s6 m6 l6">
        <div class="page-title">UC Transporter Details</div>
        <!-- <p>{{$order}} - {{$transporter}} - {{$id}}</p> -->
        <input id="transporter_order_id" type="hidden" value="{{$id}}">
    </div>
    <div class="col s6 m6 l6">
        <div class="page-title right"><a href="{{url('/order/detail/'.$order->id)}}">Back</a></div>
    </div>
</div>

<div class="row">
    <div class="col s12">
        <ul class="tabs z-depth-1">
            <li id="general_info_tab" class="tab"><a href="#general_info_div" class="active">General Info</a></li>
            <li id="vehicle_tab" class="tab disabled"><a href="#vehicle_div">Vehicle</a></li>
            <li id="load_at_tab" class="tab disabled"><a href="#load_at_div">Load At</a></li>
            <li id="offload_at_tab" class="tab disabled"><a href="#offload_at_div">Offload At</a></li>
        </ul>
    </div>
    <div id="general_info_div" class="col s12">
        <form id="general_info_form" class="row grey lighten-2">
            <div class="col s12 l7">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field col s6">
                                <input disabled id="gi_to" name="gi_to" type="text" value="{{$transporter->name}}">
                                <label for="gi_to">To</label>
                            </div>

                            <div class="input-field col s6">
                                <input disabled id="gi_from" name="gi_from" type="text" value="{{Auth::user()->name}}">
                                <label for="gi_from">From</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="gi_att" name="gi_att" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->gi_att : ''}}">
                                <label for="gi_att">Att</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Commodity</span>
                        <div class="row">
                            <div class="input-field col s4">
                                <input id="gi_desc" name="gi_desc" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->gi_desc : ''}}">
                                <label for="gi_desc">Desc</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="gi_tons" name="gi_tons" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->gi_tons : ''}}">
                                <label for="gi_tons">Tons</label>
                            </div>
                            <div class="input-field col s4">
                                <a class="waves-effect waves-light btn"><i class="material-icons">search</i></a>
                            </div>
                            <div class="input-field col s7">
                                <p class="p-v-xs">
                                @if (!empty($transporter_order_detail) && $transporter_order_detail->gi_abnormal == 1)
                                    <input type="checkbox" id="gi_abnormal" name="gi_abnormal" checked>
                                @else
                                    <input type="checkbox" id="gi_abnormal" name="gi_abnormal">
                                @endif
                                    <label for="gi_abnormal">Abnormal Load</label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Additional Instruction</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="gi_instruction" name="gi_instruction" type="text" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->gi_instruction : ''}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 l5">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field col s6">
                                <input disabled id="gi_order_no" name="gi_order_no" type="text" style="color:black;" value="{{$order->id}}">
                                <label for="gi_order_no" class="active">Order No</label>
                            </div>
                            <div class="input-field col s6">
                                <input disabled id="gi_extension" name="gi_extension" type="text" value="A">
                                <label for="gi_extension">Extension</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Goods in Transit Insurance</span>
                        <div class="row">
                            <div class="input-field col s4">
                                <p class="p-v-xs">
                                    @if (!empty($transporter_order_detail) && $transporter_order_detail->gi_reqd == 1)
                                    <input type="checkbox" id="gi_reqd" name="gi_reqd" checked>
                                    @else
                                    <input type="checkbox" id="gi_reqd" name="gi_reqd">
                                    @endif
                                    <label for="gi_reqd">Reqd</label>
                                </p>
                            </div>
                            <div class="input-field col s6">
                                <input id="gi_value" name="gi_value" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->gi_value : ''}}">
                                <label for="gi_value">Value</label>
                            </div>
                            <div class="input-field col s2">
                                <select id="gi_currency" name="gi_currency" class="browser-default" tabindex="-1">
                                    @if (!empty($transporter_order_detail))
                                    <option value="USD" {{( $transporter_order_detail->gi_currency == 'USD') ? 'selected' : '' }}>USD</option>
                                    <option value="ZAR" {{( $transporter_order_detail->gi_currency == 'ZAR') ? 'selected' : '' }}>ZAR</option>
                                    @else
                                    <option value="USD" selected>USD</option>
                                    <option value="ZAR">ZAR</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Transporter Rate</span>
                        <div class="row">
                            <div class="input-field col s8">
                                <input id="gi_rate" name="gi_rate" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->gi_rate : ''}}">
                                <label for="gi_rate">Rate (Excl VAT)</label>
                            </div>
                            <div class="input-field col s4">
                                <select id="gi_terms" name="gi_terms" class="browser-default" tabindex="-1">
                                    @if (!empty($transporter_order_detail))
                                    <option value="COD" {{( $transporter_order_detail->gi_terms == 'COD') ? 'selected' : '' }}>COD</option>
                                    <option value="30 Days" {{( $transporter_order_detail->gi_terms == '30 Days') ? 'selected' : '' }}>30 Days</option>
                                    @else
                                    <option value="COD" selected>COD</option>
                                    <option value="30 Days">30 Days</option>
                                    @endif
                                </select>
                                <label for="gi_terms" class="active">Terms of payment</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field col s12">
                                <input disabled id="gi_authorised" name="gi_authorised" type="text" value="{{Auth::user()->name}}">
                                <label for="gi_authorised">Authorised By</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="vehicle_div" class="col s12">
        <form id="vehicle_form" class="row grey lighten-2">
            <div class="col s12 l12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Vehicle Requirements</span>
                        <div class="row">
                            <div class="input-field col s6">
                                <select id="v_type_id" name="v_type_id" class="vehicle-type-list js-states browser-default" tabindex="-1" style="width: 100%"></select>
                                <label for="v_type_id" class="active">Type Of Vehicle</label>
                            </div>
                            <div class="input-field col s6">
                                <a href="#create_vehtype_modal" class="modal-trigger waves-effect waves-light btn">Add New Vehicle Type</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Dimensions</span>
                        <div class="row">
                            <div class="input-field col s2">
                                <input id="v_l" name="v_l" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->v_l : ''}}">
                                <label for="v_l">L (m)</label>
                            </div>
                            <div class="input-field col s2">
                                <input id="v_w" name="v_w" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->v_w : ''}}">
                                <label for="v_w">W (m)</label>
                            </div>
                            <div class="input-field col s2">
                                <input id="v_h" name="v_h" type="text" class="required" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->v_h : ''}}">
                                <label for="v_h">H (m)</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="v_add_dimension" name="v_add_dimension" type="text" value="{{!empty($transporter_order_detail) ? $transporter_order_detail->v_add_dimension : ''}}">
                                <label for="v_add_dimension">Additional dimensions</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Equipment Required</span>
                        <div class="row">
                            <div class="col l2"></div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_chains"><label for="gi_chains">Chains</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_plates"><label for="gi_plates">C/Plates</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_dunnage"><label for="gi_dunnage">Dunnage</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_sides"><label for="gi_sides">Sides</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_straps"><label for="gi_straps">Straps</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_tarps"><label for="gi_tarps">Tarps</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_twistlocks"><label for="gi_twistlocks">TwistLocks</label></p>
                            </div>
                            <div class="input-field col s12 l1">
                                <p class="p-v-xs"><input type="checkbox" id="gi_uprights"><label for="gi_uprights">Uprights</label></p>
                            </div>
                            <div class="col l2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="load_at_div" class="col s12">
        <form id="load_at_form" class="row grey lighten-2">
            <div class="col s12 l12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Load Address</span>
                        <div class="row">
                            <div class="input-field col s8">
                                <input id="load_addr_id" type="hidden" value="{{!empty($select_loadAddr) ? $select_loadAddr->id : ''}}">
                                <input id="l_name" name="l_name" type="text" class="required" value="{{!empty($select_loadAddr) ? $select_loadAddr->name : ''}}" placeholder="">
                                <label for="l_name">Load Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s5">
                                <input id="l_address1" name="l_address1" type="text" class="required" value="{{!empty($select_loadAddr) ? $select_loadAddr->address1 : ''}}" placeholder="">
                                <label for="l_address1">Address</label>
                            </div>
                            <div class="input-field col s3">
                                <input id="l_address2" name="l_address2" type="text" value="{{!empty($select_loadAddr) ? $select_loadAddr->address2 : ''}}" placeholder="">
                            </div>
                            <div class="input-field col s2">
                                <input id="l_address3" name="l_address3" type="text" value="{{!empty($select_loadAddr) ? $select_loadAddr->address3 : ''}}" placeholder="">
                            </div>
                            <div class="input-field col s2">
                                <a href="#load_at_modal" class="modal-trigger waves-effect waves-light btn">
                                    <i class="material-icons">search</i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s5">
                                <input id="l_city" name="l_city" type="text" class="required" value="{{!empty($select_loadAddr) ? $select_loadAddr->city : ''}}" placeholder="">
                                <label for="l_city">City</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="l_province1" name="l_province1" type="text" class="required" value="{{!empty($select_loadAddr) ? $select_loadAddr->province1 : ''}}" placeholder="">
                                <label for="l_province1">Province</label>
                            </div>
                            <div class="input-field col s3">
                                <input id="l_province2" name="l_province2" type="text" value="{{!empty($select_loadAddr) ? $select_loadAddr->province2 : ''}}" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Contact person at Load Address</span>
                        <div class="row">
                            <div class="input-field col s5">
                                <input id="l_contact" type="text" class="validate">
                                <label for="l_contact">Contact</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="l_telephone" type="text" class="validate">
                                <label for="l_telephone">Telephone</label>
                            </div>
                            <div class="input-field col s3">
                                <a class="waves-effect waves-light btn"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="offload_at_div" class="col s12">
        <form id="offload_at_form" class="row grey lighten-2">
            <div class="col s12 l8">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Offload Address</span>
                        <div class="row">
                            <div class="input-field col s10">
                                <input id="offload_addr_id" name="offload_addr_id" type="hidden" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->id : ''}}">
                                <input id="o_name" name="o_name" type="text" class="required" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->name : ''}}" placeholder="">
                                <label for="o_name">Offload Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s4">
                                <input id="o_address1" name="o_address1" type="text" class="required" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->address1 : ''}}" placeholder="">
                                <label for="o_address1">Address</label>
                            </div>
                            <div class="input-field col s3">
                                <input id="o_address2" name="o_address2" type="text" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->address2 : ''}}" placeholder="">
                            </div>
                            <div class="input-field col s3">
                                <input id="o_address3" name="o_address3" type="text" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->address3 : ''}}" placeholder="">
                            </div>
                            <div class="input-field col s2">
                                <a href="#offload_at_modal" class="modal-trigger waves-effect waves-light btn">
                                    <i class="material-icons">search</i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s5">
                                <input id="o_city" name="o_city" type="text" class="required" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->city : ''}}" placeholder="">
                                <label for="o_city">City</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="o_province1" name="o_province1" type="text" class="required" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->province1 : ''}}" placeholder="">
                                <label for="o_province1">Province</label>
                            </div>
                            <div class="input-field col s3">
                                <input id="o_province2" name="o_province2" type="text" value="{{!empty($select_offloadAddr) ? $select_offloadAddr->province2 : ''}}" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Contact person at Offload Address</span>
                        <div class="row">
                            <div class="input-field col s5">
                                <input id="o_contact" type="text" class="validate">
                                <label for="o_contact">Contact</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="o_telephone" type="text" class="validate">
                                <label for="o_telephone">Telephone</label>
                            </div>
                            <div class="input-field col s3">
                                <a class="waves-effect waves-light btn"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 l4">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">POD Received</span>
                        <div class="row">
                            <div class="input-field col s12">
                                <p class="p-v-xxl"><input type="checkbox" id="o_recd"><label for="o_recd">POD Recd</label></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="input-field col s12">
                                <a class="waves-effect waves-light btn" target="_blank" onclick="makePDF()">
                                    <i class="material-icons left">print</i>Print Load Confirmation
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="create_vehtype_modal" class="modal modal-footer">
    <form id="vehtype_form" class="col s12">
        <div class="modal-content">
            <h4>New Vehicle Type</h4>
            <div class="row">
                <div class="input-field col s12">
                    <input id="vehicle_type_name" name="vehicle_type_name" type="text" class="required">
                    <label for="vehicle_type_name">Type Name</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a class="modal-action waves-effect waves-green btn-flat" onclick="createVehicleType()">Create</a>
        </div>
    </form>
</div>

<div id="load_at_modal" class="modal modal-footer">
    <form class="col s12">
        <div class="modal-content">
            <h4>Load At List</h4>
            <div class="row">
                <div class="col s12 m12 l12">
                    <table id="loadAddrTable" class="display responsive-table">
                        <thead>
                            <tr>
                                <th style="display:none;">Id</th>
                                <th style="display:none;">address1</th>
                                <th style="display:none;">address2</th>
                                <th style="display:none;">address3</th>
                                <th style="display:none;">province1</th>
                                <th style="display:none;">province2</th>
                                <th>Load Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Province</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($loadAddrs as $loadAddr)
                                <tr>
                                    <td style="display:none;">{{$loadAddr->id}}</td>
                                    <td style="display:none;">{{$loadAddr->address1}}</td>
                                    <td style="display:none;">{{$loadAddr->address2}}</td>
                                    <td style="display:none;">{{$loadAddr->address3}}</td>
                                    <td style="display:none;">{{$loadAddr->province1}}</td>
                                    <td style="display:none;">{{$loadAddr->province2}}</td>
                                    <td>{{$loadAddr->name}}</td>
                                    <td>{{$loadAddr->address1}} {{$loadAddr->address2}} {{$loadAddr->address3}}</td>
                                    <td>{{$loadAddr->city}}</td>
                                    <td>{{$loadAddr->province1}} {{$loadAddr->province2}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a id="load_loadAddr" class="modal-action modal-close waves-effect waves-green btn-flat">Load</a>
        </div>
    </form>
</div>

<div id="offload_at_modal" class="modal modal-footer">
    <form class="col s12">
        <div class="modal-content">
            <h4>Offload At List</h4>
            <div class="row">
                <div class="col s12 m12 l12">
                    <table id="offloadAddrTable" class="display responsive-table">
                        <thead>
                            <tr>
                                <th style="display:none;">Id</th>
                                <th style="display:none;">address1</th>
                                <th style="display:none;">address2</th>
                                <th style="display:none;">address3</th>
                                <th style="display:none;">province1</th>
                                <th style="display:none;">province2</th>
                                <th>Offload Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Province</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($offLoadAddrs as $offLoadAddr)
                                <tr>
                                    <td style="display:none;">{{$offLoadAddr->id}}</td>
                                    <td style="display:none;">{{$offLoadAddr->address1}}</td>
                                    <td style="display:none;">{{$offLoadAddr->address2}}</td>
                                    <td style="display:none;">{{$offLoadAddr->address3}}</td>
                                    <td style="display:none;">{{$offLoadAddr->province1}}</td>
                                    <td style="display:none;">{{$offLoadAddr->province2}}</td>
                                    <td>{{$offLoadAddr->name}}</td>
                                    <td>{{$offLoadAddr->address1}} {{$offLoadAddr->address2}} {{$offLoadAddr->address3}}</td>
                                    <td>{{$offLoadAddr->city}}</td>
                                    <td>{{$offLoadAddr->province1}} {{$offLoadAddr->province2}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a id="offload_loadAddr" class="modal-action modal-close waves-effect waves-green btn-flat">Load</a>
        </div>
    </form>
</div>

@push('javascript')
<script>
$(document).ready(function() {
    $('#gi_value').inputmask("Regex", { regex: "^[0-9]{0,9}(\.[0-9]{1,2})?$" });
    $('#gi_rate').inputmask("Regex", { regex: "^[0-9]{0,9}(\.[0-9]{1,2})?$" });

    $('#general_info_form').validate({
        rules:{
            gi_att: "required",
            gi_desc: "required",
            gi_tons: {required:true, number:true},
            gi_value: {required:true, number:true},
            gi_rate: {required:true, number:true}
        }
    });

    $('#vehicle_form').validate({
        rules:{
            v_l: {required:true, number:true},
            v_w: {required:true, number:true},
            v_h: {required:true, number:true}
        }
    });

    $('#load_at_form').validate({
        rules:{
            l_name: "required",
            l_address1: "required",
            l_city: "required",
            l_province1: "required"
        }
    });

    $('#offload_at_form').validate({
        rules:{
            o_name: "required",
            o_address1: "required",
            o_city: "required",
            o_province1: "required"
        }
    });

    $('#vehtype_form').validate({
        rules:{
            vehicle_type_name: "required",
        }
    });

    $('#vehicle_tab').click(function(){
        if($('#general_info_form').valid()){
            $('#vehicle_tab').removeClass('disabled');
        }
    });

    $('#load_at_tab').click(function(){
        if($('#vehicle_form').valid()){
            $('#load_at_tab').removeClass('disabled');
        }
    });

    $('#offload_at_tab').click(function(){
        if($('#load_at_form').valid()){
            $('#offload_at_tab').removeClass('disabled');
        }
    });

    var load_confirmation_table = $('#load_confirmation_table').DataTable({
        scrollY: '100px',
        scrollCollapse: true,
        paging: false,
        "searching": false,
    });

    var loadAddrTable = $('#loadAddrTable').DataTable({
        language: {
            searchPlaceholder: 'Search Addresses',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>'
            }
        }
    });

    var offloadAddrTable = $('#offloadAddrTable').DataTable({
        language: {
            searchPlaceholder: 'Search Addresses',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>'
            }
        }
    });

    $('.dataTables_length select').addClass('browser-default');

    $.ajax({
        url: "{{url('vehicleType/all')}}",
        type: "GET",
        success: function(response) {
            if(response['success']){
                var vehicle_types = response['success'].map(item => { return { id:item.id, text:item.name }});
                $(".vehicle-type-list").select2({ data: vehicle_types });
                $('#v_type_id').select2().select2("val", '{{!empty($transporter_order_detail) ? $transporter_order_detail->v_type_id : 1}}');
            }
            else{
                $(".vehicle-type-list").select2({ data: null });
            }
        }
    });

    $('#loadAddrTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            loadAddrTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#offloadAddrTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            offloadAddrTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#load_loadAddr').click(function () {
        var record = $.map(loadAddrTable.rows('.selected').data(), function (item) { return item; });
        $('#load_addr_id').val(record[0]);
        $('#l_name').val(record[6]);
        $('#l_address1').val(record[1]);
        $('#l_address2').val(record[2]);
        $('#l_address3').val(record[3]);
        $('#l_city').val(record[8]);
        $('#l_province1').val(record[4]);
        $('#l_province2').val(record[5]);
    });

    $('#offload_loadAddr').click(function () {
        var record = $.map(offloadAddrTable.rows('.selected').data(), function (item) { return item; });
        $('#offload_addr_id').val(record[0]);
        $('#o_name').val(record[6]);
        $('#o_address1').val(record[1]);
        $('#o_address2').val(record[2]);
        $('#o_address3').val(record[3]);
        $('#o_city').val(record[8]);
        $('#o_province1').val(record[4]);
        $('#o_province2').val(record[5]);
    });
});

function createVehicleType()
{
    if(!$('#vehtype_form').valid()){
        return;
    }

    var vehicle_type_name = $('#vehicle_type_name').val();

    $.ajax({
        url: "{{url('vehicleType/create')}}",
        type: "POST",
        data: {vehicle_type_name},
        success: function(response) {
            if(response['success'] == 1){
                Materialize.toast('Success - Saved!!!', 2000, 'green rounded');
                location.reload();
            }
            else{
                Materialize.toast('Error- Not Saved!!!', 2000, 'red rounded');
            }
        }
    });
}

function makePDF()
{
    if(!$('#offload_at_form').valid())
    {
        return;
    }

    var info = [];
    var transporter_order_id = $('#transporter_order_id').val();

    var gi_att = $('#gi_att').val();
    var gi_desc = $('#gi_desc').val();
    var gi_tons = $('#gi_tons').val();
    var gi_abnormal = 0;
    if ($('#gi_abnormal').is(":checked"))
    {
        gi_abnormal = 1;
    }
    var gi_instruction = $('#gi_instruction').val();
    var gi_reqd = 0;
    if ($('#gi_reqd').is(":checked"))
    {
        gi_reqd = 1;
    }
    var gi_value = $('#gi_value').val();
    var gi_currency = $("#gi_currency option:selected").val();

    var gi_rate = $('#gi_rate').val();
    var gi_terms = $("#gi_terms option:selected").val();

    var v_type_id = $("#v_type_id option:selected").val();
    var v_l = $('#v_l').val();
    var v_w = $('#v_w').val();
    var v_h = $('#v_h').val();
    var v_add_dimension = $('#v_add_dimension').val();

    var load_addr_id = $('#load_addr_id').val();
    var offload_addr_id = $('#offload_addr_id').val();

    info.push(transporter_order_id);
    info.push(gi_att);
    info.push(gi_desc);
    info.push(gi_tons);
    info.push(gi_abnormal);
    info.push(gi_instruction);
    info.push(gi_reqd);
    info.push(gi_value);
    info.push(gi_currency);
    info.push(gi_rate);
    info.push(gi_terms);
    info.push(v_type_id);
    info.push(v_l);
    info.push(v_w);
    info.push(v_h);
    info.push(v_add_dimension);
    info.push(load_addr_id);
    info.push(offload_addr_id);

    $.ajax({
        url: "{{url('transporter/saveConfirmData')}}",
        type: "POST",
        data: {info},
        success: function(response) {
            if(response['success'] > 0){
                window.open("/transporter/pdf/" + transporter_order_id, "_blank");
            }
            else{
                Materialize.toast('Error- Not Saved!!!', 2000, 'red rounded');
            }
        }
    });
}
</script>
@endpush

@push('javascript')
    <script src="{{asset('plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
    <script src="{{asset('plugins/google-code-prettify/prettify.js')}}"></script>
    <script src="{{asset('js/pages/form-input-mask.js')}}"></script>
    <script src="{{asset('plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/pages/ui-modals.js')}}"></script>
    <script src="{{asset('plugins/select2/js/select2.min.js')}}"></script>
@endpush
@endsection
