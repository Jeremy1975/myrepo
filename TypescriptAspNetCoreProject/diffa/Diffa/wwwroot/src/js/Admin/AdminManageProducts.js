var AdminManageProducts = /** @class */ (function () {
    function AdminManageProducts() {
        this.promoDateValues = new Map();
    }
    AdminManageProducts.prototype.LoadProducts = function (id) {
        var _this = this;
        $("#liProducts").addClass("active");
        $("#ProductList").load("/admin/products?eventId=" + id, function () {
            $("#js-add-product").on("click", function (e) { return _this.AddProduct(e); });
            $(".js-edit-product").on("click", function (e) { return _this.EditProduct(e); });
            $(".js-delete-product").on("click", function (e) { return _this.DeleteProduct(e); });
        });
    };
    AdminManageProducts.prototype.AddProduct = function (e) {
        $("#product-main-container").hide();
        $("#product-modal-container").load("/admin/products/add-product", function () {
            console.log("add......");
        });
    };
    AdminManageProducts.prototype.EditProduct = function (e) {
        $("#product-main-container").hide();
        $("#product-modal-container").load("/admin/products/edit-product?productId=" + $(e.currentTarget).data("id"), function () {
            console.log("edit..........");
        });
    };
    AdminManageProducts.prototype.DeleteProduct = function (e) {
        if (confirm("Are you sure you want to delete this product?"))
            window.location.href = "/admin/products/delete-product?productId=" + $(e.currentTarget).data("id");
    };
    return AdminManageProducts;
}());
//# sourceMappingURL=AdminManageProducts.js.map