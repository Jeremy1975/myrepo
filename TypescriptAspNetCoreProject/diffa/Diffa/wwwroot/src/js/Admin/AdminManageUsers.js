var AdminManageUsers = /** @class */ (function () {
    function AdminManageUsers() {
        this.instance = null;
        this.instance = this;
    }
    AdminManageUsers.prototype.LoadStaffDetails = function () {
        var _this = this;
        $("#liUsers").addClass("active");
        $("#StaffDetails").load("/admin/user-detail/staff-details", function () {
            $("#js-add-staff").click(function (e) { return _this.LoadAddStaff(e); });
            $(".js-edit-staff").click(function (e) { return _this.EditStaff(e); });
            $(".js-delete-staff").click(function (e) { return _this.DeleteStaff(e); });
        });
    };
    AdminManageUsers.prototype.DeleteStaff = function (e) {
        var _this = this;
        e.preventDefault();
        if (confirm("Are you sure you want to delete this user")) {
            $.ajax({
                type: "POST",
                url: $(e.currentTarget).data("href"),
                data: "userId=" + $(e.currentTarget).data("user-id"),
                success: function (data) {
                    if (data.success) {
                        _this.LoadStaffDetails();
                    }
                    else {
                        alert(data.message);
                    }
                },
                error: function (data) { alert(data); },
                complete: function () {
                    $("#js-save").attr("disabled", "false");
                }
            });
        }
    };
    AdminManageUsers.prototype.LoadAddStaff = function (e) {
        var _this = this;
        $("#main-container").hide();
        $("#modal-container").load("/admin/user-detail/add-staff", function () {
            _this.BindStaffForm();
        });
    };
    AdminManageUsers.prototype.EditStaff = function (e) {
        var _this = this;
        $("#main-container").hide();
        $("#modal-container").load($(e.currentTarget).data("href"), function () {
            _this.BindStaffForm();
        });
    };
    AdminManageUsers.prototype.BindStaffForm = function () {
        var _this = this;
        $.validator.unobtrusive.parse("#js-staff-form");
        $("#js-cancel").on("click", function () { _this.CancelModal(); });
        $("#js-staff-form").submit(function (e) { return _this.SubmitForm(e); });
        $("#PhoneNumber").mask('000-000-0000');
    };
    AdminManageUsers.prototype.CancelModal = function () {
        $("#modal-container").html("");
        $("#main-container").show();
    };
    ;
    AdminManageUsers.prototype.SubmitForm = function (e) {
        var _this = this;
        e.preventDefault();
        var form = $(e.currentTarget);
        if (!form.valid())
            return;
        $("#js-save").attr("disabled", "true");
        $.ajax({
            type: "POST",
            url: form.data("url"),
            data: form.serialize(),
            success: function (data) {
                if (data.success) {
                    _this.CancelModal();
                    _this.LoadStaffDetails();
                }
                else {
                    alert(data.message);
                }
            },
            error: function (data) { alert(data); },
            complete: function () {
                $("#js-save").attr("disabled", "false");
            }
        });
    };
    ;
    return AdminManageUsers;
}());
//# sourceMappingURL=AdminManageUsers.js.map