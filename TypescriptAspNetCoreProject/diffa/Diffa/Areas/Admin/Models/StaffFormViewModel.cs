﻿using Diffa.Data.Models;
using Diffa.Data.Transformed;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Models
{
    public class StaffFormViewModel
    {
        public Guid? Id { get; set; }

        [DisplayName("First Name")]
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [DisplayName("Email")]
        [Required]
        [StringLength(100)]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("Phone Number")]
        [Required]
        [StringLength(12, ErrorMessage = "Please provide a valid phone number", MinimumLength = 12)]
        [Phone]
        public string PhoneNumber { get; set; }

        public List<SelectableRoles> Roles { get; set; }
    }
}
