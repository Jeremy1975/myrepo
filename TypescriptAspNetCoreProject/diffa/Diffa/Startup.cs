using Diffa.Areas.Admin.Helpers;
using Diffa.Areas.Home.Helpers;
using Diffa.Config;
using Diffa.Data;
using Diffa.Data.Extensions;
using Diffa.Data.Models;
using Diffa.Data.Repos;
using Diffa.Logging;
using Diffa.Policies;
using Diffa.Policies.Claims;
using Diffa.Policies.Handlers;
using Diffa.Policies.Requirements;
using Diffa.Seeding;
using Diffa.Services;
using Diffa.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer().AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddClaimsPrincipalFactory<AdministratorRoleAccessClaim>();

            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();

            services.AddRazorPages();

            AddPolicies(services);
            AddControllerHelpers(services);

            var sendGridCredentials = Configuration.GetSection("SendGridCredentials").Get<SendGridCredentials>();

            services.AddSingleton(sendGridCredentials);

            services.AddTransient<IEmailSender, SendGridEmailSender>();
            services.AddTransient<IEmailTemplatedSender, SendGridEmailSender>();
            services.AddHttpContextAccessor();
            services.AddScoped<ILoggingRepo, LoggingRepo>();
            //services.AddScoped<ILogger, DbLogger>();
            //services.AddSingleton<ILoggerProvider, LoggerDatabaseProvider>();

        }

     

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,UserManager<ApplicationUser> userManager,RoleManager<ApplicationRole> roleManager, ApplicationDbContext dbContext, ILoggerFactory loggerFactory) //, ApplicationDbContext dbContext
        {
            var dbLoggerProvider = new LoggerDatabaseProvider(app);
            app.ConfigureExceptionHandler(dbLoggerProvider.CreateLogger("Global"));

            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                //app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });


           
            loggerFactory.AddProvider(dbLoggerProvider);

            SeedDataInitializer.SeedData(userManager, roleManager, dbContext);
        }

        private void AddPolicies(IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyNames.UserManagement, policy =>
                    policy.Requirements.Add(new AdminstratorRoleAccessRequirement(PolicyNames.UserManagement)));
            });

            services.AddSingleton<IAuthorizationHandler, AdministratorRoleAccessHandler>();
        }

        private void AddControllerHelpers(IServiceCollection services)
        {
            services.AddScoped<UserDetailsHelper, UserDetailsHelper>();
            services.AddScoped<EventsHelper, EventsHelper>();
            services.AddScoped<HomeHelper, HomeHelper>();
            services.AddScoped<IEventsRepo, EventsRepo>();
            services.AddScoped<TablesHelper, TablesHelper>();
            services.AddScoped<ITablesRepo, TablesRepo>();
            services.AddScoped<ProductsHelper, ProductsHelper>();
            services.AddScoped<IProductsRepo, ProductsRepo>();
        }
    }
}
