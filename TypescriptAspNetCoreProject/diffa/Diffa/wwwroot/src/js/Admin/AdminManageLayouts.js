var AdminManageLayouts = /** @class */ (function () {
    function AdminManageLayouts(eventId) {
        this.eventId = eventId;
        console.log(eventId);
    }
    AdminManageLayouts.prototype.Init = function () {
        this.SetupDraggables();
        $(".addMe").on("click", function (e) {
            $(".draggable-selected").css("background-color", $(e.target).css("color"));
        });
    };
    AdminManageLayouts.prototype.SetupDraggables = function () {
        var _this = this;
        $(".draggable,.draggable-element").each(function (index, element) {
            $(element).offset({ top: $(element).data("initialOffsetTop"), left: $(element).data("initialOffsetLeft") });
        });
        $('.draggable').on("click", function (e) {
            $(".draggable-selected").removeClass("draggable-selected");
            $(e.target).addClass("draggable-selected");
            _this.selectedTableId = $(e.target).data("tableId");
            _this.selectedTableIndex = $(e.target).data("tableIndex");
            $("#Tables").show();
        });
        $('.draggable').kendoDraggable({
            hint: function (original) {
                return original.clone().addClass("drag-clone");
            },
            dragstart: function (e) {
                if (!$(e.originalEvent.target).hasClass("draggable"))
                    return;
                $(e.originalEvent.target).addClass("drag-hide");
            }
        });
        $('.draggable-element').kendoDraggable({
            hint: function (original) {
                return original.clone().addClass("drag-clone");
            },
            dragstart: function (e) {
                if (!$(e.originalEvent.target).hasClass("draggable-element"))
                    return;
                $(e.originalEvent.target).addClass("drag-hide");
            }
        });
        $('body').kendoDropTarget({
            drop: function (e) {
                var pos = $(".drag-clone").offset();
                $(e.draggable.currentTarget)
                    .removeClass("drag-hide")
                    .offset(pos);
                var url = "";
                var data = {};
                if (e.draggable.currentTarget.data("tableId") == undefined) {
                    url = "/admin/tables/table-layout-element-set";
                    data = {
                        eventId: _this.eventId,
                        elementName: e.draggable.currentTarget.attr("id"),
                        top: pos.top,
                        left: pos.left
                    };
                }
                else {
                    url = "/admin/tables/table-layout-set";
                    data = {
                        tableId: e.draggable.currentTarget.data("tableId"),
                        tableIndex: e.draggable.currentTarget.data("tableIndex"),
                        top: pos.top,
                        left: pos.left
                    };
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data
                });
            }
        });
    };
    return AdminManageLayouts;
}());
//# sourceMappingURL=AdminManageLayouts.js.map