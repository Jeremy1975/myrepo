<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VehicleType;

class VehicleTypeController extends Controller
{
    public function getAllVehicleTypes()
    {
        $vehicleTypes = VehicleType::select('*')->get();
        return response()->json(['success' => $vehicleTypes]);
    }

    public function create(Request $request)
    {
        $vehicle_type_name = $request->vehicle_type_name;

        $result = 0;
        try{
            $new_vehicle_type = VehicleType::create(
            [
                'name' => $vehicle_type_name
            ]);

            if($new_vehicle_type){
                $result = 1;
            }
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }
}
