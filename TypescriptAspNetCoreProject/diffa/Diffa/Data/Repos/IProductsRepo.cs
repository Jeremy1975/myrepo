﻿using Diffa.Areas.Admin.Models;
using System;
using System.Collections.Generic;

namespace Diffa.Data.Repos
{
    public interface IProductsRepo
    {
        bool AddProduct(ProductFormViewModel productForm);
        List<ProductFormViewModel> GetProducts(Guid eventId);
        ProductFormViewModel GetProduct(Guid productId);
        bool EditProduct(ProductFormViewModel model);
        void DeleteProduct(Guid productId);
    }
}
