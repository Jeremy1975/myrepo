﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Extensions;
using Diffa.Data.Models.DiffaEvents;
using Diffa.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Diffa.Data.Repos
{
    public class EventsRepo : IEventsRepo
    {
        ApplicationDbContext dbContext;
        private IWebHostEnvironment _environment;
        public EventsRepo(ApplicationDbContext dbContext, IWebHostEnvironment environment)
        {
            this.dbContext = dbContext;
            _environment = environment;
        }
        public bool AddEvent(EventFormViewModel eventForm)
        {
            var newEvent = eventForm.ToDiffaEvent();
            newEvent.EventId = Guid.NewGuid();
            if (eventForm.EventLogo != null)
            {
                string path;
                if (FileUtility.SaveFile(newEvent.EventId, "Logo", eventForm.EventLogo, out path, _environment))
                    newEvent.LogoPath = path;
            }

            this.dbContext.Add(newEvent);
            this.dbContext.SaveChanges();
            return true;
        }

        public void DeleteEvent(Guid eventId)
        {
            var existing = this.dbContext.DiffaEvents.Where(p => p.EventId == eventId).FirstOrDefault();
            if (existing != null)
            {
                existing.Deleted = true;
                this.dbContext.Update(existing);
                this.dbContext.SaveChanges();
            }
        }

        public bool EditEvent(EventFormViewModel model)
        {
            var existing=this.dbContext.DiffaEvents.Where(p => p.EventId == model.EventId).FirstOrDefault();
            if (existing != null)
            {
                var clonedForSave = existing.ToEdited(model);
                if (model.EventLogo != null)
                {
                    string path;
                    if(FileUtility.SaveFile(model.EventId, "Logo", model.EventLogo, out path, _environment))
                        clonedForSave.LogoPath = path;
                }

                this.dbContext.Update(clonedForSave);
                this.dbContext.SaveChanges();
                return true;
            }

            return false;
        }

        public EventFormViewModel GetEvent(Guid eventId)
        {
            return this.GetLiveEvents().Where(p => p.EventId == eventId).FirstOrDefault()?.ToEventFormViewModel();
        }

        public List<EventFormViewModel> GetEvents()
        {
            var events = GetLiveEvents().ToList();
            return events.Select(p => p.ToEventFormViewModel()).ToList();
        }

        private IQueryable<DiffaEvent> GetLiveEvents()
        {
            return this.dbContext.DiffaEvents.Include(u=>u.DiscountCodes).Where(p => !p.Deleted);
        }

        public void PublishEvent(Guid eventId)
        {
            var existing = this.dbContext.DiffaEvents.Where(p => p.EventId == eventId && !p.PublishedDate.HasValue).FirstOrDefault();
            if (existing != null)
            {
                existing.PublishedDate = DateTime.Now;
                this.dbContext.Update(existing);
                this.dbContext.SaveChanges();
            }
        }
    }
}
