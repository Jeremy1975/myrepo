﻿using Diffa.Data;
using Diffa.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Diffa.Policies.Claims
{
    public class AdministratorRoleAccessClaim: UserClaimsPrincipalFactory<ApplicationUser, ApplicationRole>
    {
        private readonly ApplicationDbContext applicationDbContext;
        private readonly UserManager<ApplicationUser> userManager;

        public AdministratorRoleAccessClaim(
        UserManager<ApplicationUser> userManager,
        RoleManager<ApplicationRole> roleManager,
        IOptions<IdentityOptions> optionsAccessor,
        ApplicationDbContext applicationDbContext
        )
            : base(userManager, roleManager,optionsAccessor)
        {
            this.applicationDbContext = applicationDbContext;
            this.userManager = userManager;
        }

        async protected override Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
        {
            var identity = await base.GenerateClaimsAsync(user);
        

            var roles=await this.userManager.GetRolesAsync(user);
            foreach(var role in roles)
            {
                var modules = this.applicationDbContext.RoleModules;
                foreach(var module in modules)
                {
                    var applicationModule=this.applicationDbContext.ApplicationModules.Where(p => p.Id == module.ModuleId).FirstOrDefault();
                    if(applicationModule!=null)
                        identity.AddClaim(new Claim(PolicyNames.AdminstratorRoleAccessClaim, applicationModule.ModuleName));
                }
            }
            
            return identity;
        }
    }
}
