﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Services.Interfaces
{
    public interface IEmailTemplatedSender: IEmailSender
    {
        string MergeTemplate(string masterName, string templateName,Dictionary<string, string> replacements);
    }
}
