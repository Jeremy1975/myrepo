﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.DiffaEvents
{
    public class TablePosition
    {
        public Guid TableId { get; set; }
        public int TableIndex{ get; set; }
        public double Top { get; set; }
        public double Left { get; set; }
    }
}
