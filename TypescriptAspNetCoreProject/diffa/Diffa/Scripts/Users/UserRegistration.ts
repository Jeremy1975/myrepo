﻿/// <reference types="jquery" />

class UserRegistration {

    instance = null;
    phoneNumber:string = "";
    constructor() {
        this.instance = this;
    }

    init() {
        $("#Input_PhoneNumber").mask('000-000-0000');

        $("#Input_PhoneNumber").on("keyup", (e) => {
            this.phoneNumber = $(e.target).val().toString();
            console.log(this.phoneNumber);
        });
    }
}

var userRegistration = new UserRegistration();
userRegistration.init();