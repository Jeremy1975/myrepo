﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models
{
    public class InformationLog
    {
        [Key]
        public Guid Id { get; set; }
        public string CategoryName { get; set; }
        public string Msg { get; set; }
        public string User { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
