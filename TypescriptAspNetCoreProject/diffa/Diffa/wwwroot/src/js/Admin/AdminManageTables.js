var AdminManageTables = /** @class */ (function () {
    function AdminManageTables() {
        this.promoDateValues = new Map();
    }
    AdminManageTables.prototype.LoadEvents = function (id) {
        var _this = this;
        $("#liTables").addClass("active");
        $("#TableList").load("/admin/tables?eventId=" + id, function () {
            $("#js-add-table").on("click", function (e) { return _this.AddTable(e); });
            $(".js-edit-table").on("click", function (e) { return _this.EditTable(e); });
            $(".js-delete-table").on("click", function (e) { return _this.DeleteTable(e); });
            $("#js-view-layout").on("click", function (e) {
                _this.LoadLayout(e, id);
            });
            _this.eventId = id;
        });
    };
    AdminManageTables.prototype.LoadLayout = function (e, id) {
        $("#table-main-container").hide();
        $("#table-modal-container").load("/admin/tables/table-layout?eventId=" + id, function () {
        });
    };
    AdminManageTables.prototype.AddTable = function (e) {
        $("#table-main-container").hide();
        $("#table-modal-container").load("/admin/tables/add-table?eventId=" + this.eventId, function () {
            $("#HexColor").kendoColorPicker({
                preview: false,
                value: "#ffffff",
                buttons: false
            });
        });
    };
    AdminManageTables.prototype.EditTable = function (e) {
        $("#table-main-container").hide();
        $("#table-modal-container").load("/admin/tables/edit-table?tableId=" + $(e.currentTarget).data("id"), function () {
            $("#HexColor").kendoColorPicker({
                preview: false,
                value: "#ffffff",
                buttons: false
            });
        });
    };
    AdminManageTables.prototype.DeleteTable = function (e) {
        if (confirm("Are you sure you want to delete this table?"))
            window.location.href = "/admin/tables/delete-table?tableId=" + $(e.currentTarget).data("id");
    };
    return AdminManageTables;
}());
//# sourceMappingURL=AdminManageTables.js.map