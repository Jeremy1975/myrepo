﻿using Diffa.Areas.Admin.Helpers;
using Diffa.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin
{
    [Route("admin/events")]
    [Area("Admin")]
    public class EventsController: Controller
    {
        EventsHelper helper;
        ProductsHelper _productsHelper;
        TablesHelper _tablesHelper;

        public EventsController(EventsHelper helper, ProductsHelper productsHelper, TablesHelper tablesHelper)
        {
            this.helper = helper;
            _productsHelper = productsHelper;
            _tablesHelper = tablesHelper;
        }
        [Route("events")]
        [Authorize(Roles = "Administrator")]
        public ActionResult Events()
        {
            List<EventFormViewModel> events= this.helper.GetEvents();
            ViewBag.Products = _productsHelper.GetProducts(Guid.Empty);
            ViewBag.Tables = _tablesHelper.GetEventTables(Guid.Empty);

            return View(events);
        }

        [Route("add-event")]
        [Authorize(Roles = "Administrator")]
        public ActionResult AddEvent()
        {
            EventFormViewModel model = new EventFormViewModel();
            return View(model);
        }

        [Route("edit-event")]
        [Authorize(Roles = "Administrator")]
        public ActionResult EditEvent(Guid eventId)
        {
            EventFormViewModel model = this.helper.GetEvent(eventId);
            return View(model);
        }

        [Route("edit-event")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult EditEvent(EventFormViewModel model)
        {
            var success = this.helper.EditEvent(model);
            if (success)
                return new RedirectResult("/admin/manage-events");

            return View(model);
        }

        [Route("add-event")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddEvent(EventFormViewModel model)
        {
            var success = this.helper.AddEvent(model);
            if (success)
                return new RedirectResult("/admin/manage-events");

            return View(model);
        }

        [Route("delete-event")]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteEvent(Guid eventId)
        {
            this.helper.DeleteEvent(eventId);
            return new RedirectResult("/admin/manage-events");
        }

        [Route("publish-event")]
        [Authorize(Roles = "Administrator")]
        public ActionResult PublishEvent(Guid eventId)
        {
            this.helper.PublishEvent(eventId);
            return new RedirectResult("/admin/manage-events");
        }

        [Route("tables-event")]
        [Authorize(Roles = "Administrator")]
        public ActionResult TablesEvent(Guid eventId)
        {
            var path = string.Format("/admin/manage-tables?eventId={0}", eventId);
            return new RedirectResult(path);
        }

        [Route("products-event")]
        [Authorize(Roles = "Administrator")]
        public ActionResult ProductsEvent(Guid eventId)
        {
            var path = string.Format("/admin/manage-products?eventId={0}", eventId);
            return new RedirectResult(path);
        }
    }
}
