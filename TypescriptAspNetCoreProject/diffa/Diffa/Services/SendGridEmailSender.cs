﻿using Diffa.Config;
using Diffa.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Diffa.Services
{
    public class SendGridEmailSender : IEmailTemplatedSender
    {
        private readonly SendGridCredentials credentials;
        private IWebHostEnvironment env;
        IHttpContextAccessor httpContextAccessor;
        public SendGridEmailSender(SendGridCredentials credentials, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor)
        {
            this.credentials = credentials;
            this.env = env;
            this.httpContextAccessor = httpContextAccessor;
        }


        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute(this.credentials.SendGridKey, subject, message, email);
        }

        public Task Execute(string apiKey, string subject, string message, string email)
        {
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(this.credentials.DefaultSender),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));

            msg.SetClickTracking(false, false);
            var result = client.SendEmailAsync(msg).Result;

            return Task.CompletedTask;
        }

        public string MergeTemplate(string masterName, string templateName, Dictionary<string, string> replacements)
        {
           
            string master = GetTemplateString(masterName);
            string template = GetTemplateString(templateName);

            master = master.Replace("{{Master.Template}}", template);
            master = MergeCommon(master);
            foreach (var replacement in replacements)
            {
                master = master.Replace("{{" + replacement.Key + "}}", replacement.Value);
            }

            return master;
        }

        private string MergeCommon(string master)
        {
            master = master.Replace("{{Master.Title}}", "Diffa");
            string baseUrl = $"{httpContextAccessor.HttpContext.Request.Scheme}://{httpContextAccessor.HttpContext.Request.Host}";
            master = master.Replace("{{Master.BaseUrl}}", baseUrl) ;
            return master;
        }

        private string GetTemplateString(string templateName)
        {
            try
            {
                var webRoot = env.WebRootPath; //get wwwroot Folder
                var pathToTemplate = webRoot
                                + Path.DirectorySeparatorChar.ToString()
                                + "templates"
                                + Path.DirectorySeparatorChar.ToString()
                                + "emailTemplates"
                                + Path.DirectorySeparatorChar.ToString()
                                + $"{templateName}.html";

                using (StreamReader SourceReader = System.IO.File.OpenText(pathToTemplate))
                {
                    return SourceReader.ReadToEnd();

                }
            }
            catch
            {
                return String.Empty;
            }

        }
    }    
}