﻿class AdminManageProducts {
    promoDateValues: Map<string, Date>
    eventId: string
    constructor() {
        this.promoDateValues = new Map<string, Date>();
    }
    LoadProducts(id:string) {
        $("#liProducts").addClass("active");
        $("#ProductList").load("/admin/products?eventId=" + id, () => {
            $("#js-add-product").on("click", (e) => this.AddProduct(e));
            $(".js-edit-product").on("click", (e) => this.EditProduct(e));
            $(".js-delete-product").on("click", (e) => this.DeleteProduct(e));
        });
    }

    AddProduct(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#product-main-container").hide();
        $("#product-modal-container").load("/admin/products/add-product", () => {
            console.log("add......")
        });
    }

    EditProduct(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#product-main-container").hide();
        $("#product-modal-container").load("/admin/products/edit-product?productId=" + $(e.currentTarget).data("id"), () => {
            console.log("edit..........")
        });
    }

    DeleteProduct(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        if (confirm("Are you sure you want to delete this product?"))
            window.location.href = "/admin/products/delete-product?productId=" + $(e.currentTarget).data("id")
    }
}