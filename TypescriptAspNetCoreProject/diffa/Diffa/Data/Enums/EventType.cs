﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Enums
{
    public enum EventType
    {
        [Description("Individual Seat Arrangement")]
        IndividualSeatArrangement,
        [Description("Table Arrangement")]
        TableArrangement
    }
}
