﻿using Diffa.Data.Models;
using Diffa.Data.Models.DiffaEvents;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diffa.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole,Guid>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ApplicationModule> ApplicationModules { get; set; }
        public DbSet<RoleModule> RoleModules { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<InformationLog> InformationLogs { get; set; }
        public DbSet<DiscountCode> DiscountCodes { get; set; }
        public DbSet<DiffaEvent> DiffaEvents { get; set; }
        public DbSet<DiffaEventTable> DiffaEventTable { get; set; }
        public DbSet<TablePosition> TablePositions { get; set; }
        public DbSet<DiffaProduct> DiffaProducts { get; set; }
        public DbSet<EventElementPosition> EventElementPositions { get; set; }
        

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<TablePosition>().HasKey(c => new { c.TableId, c.TableIndex });
            builder.Entity<EventElementPosition>().HasKey(c => new { c.EventId, c.ElementName });

            base.OnModelCreating(builder);
        }
    }
}
