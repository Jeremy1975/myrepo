﻿using Diffa.Data.Models.DiffaEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Models
{
    public class TableLayoutViewModel
    {
        public List<TableFormViewModel> Tables { get; set; }
        public List<TablePosition> Positions { get; set; }
        public List<EventElementPosition> ElementPositions { get; set; }
        public Guid EventId { get; set; }
    }
}
