﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Models.DiffaEvents;

namespace Diffa.Data.Extensions
{
    public static class DiffaProductExtensions
    {
        public static ProductFormViewModel ToProductFormViewModel(this DiffaProduct product)
        {
            return new ProductFormViewModel
            {
                ProductId = product.ProductId,
                Name = product.ProductName,
                Price = product.ProductPrice,
                LogoPath = product.LogoPath,
                EventId = product.DiffaEventEventId
            };
        }

        public static DiffaProduct ToDiffaProduct(this ProductFormViewModel productForm)
        {
            return new DiffaProduct
            {
                ProductId = productForm.ProductId,
                ProductName = productForm.Name,
                ProductPrice = productForm.Price,
                LogoPath = productForm.LogoPath,
                DiffaEventEventId = productForm.EventId
            };
        }

        public static DiffaProduct ToEdited(this DiffaProduct productForm, ProductFormViewModel changes)
        {
            productForm.ProductId = changes.ProductId;
            productForm.ProductName = changes.Name;
            productForm.ProductPrice = changes.Price;
            productForm.DiffaEventEventId = changes.EventId;
            return productForm;
        }
    }
}
