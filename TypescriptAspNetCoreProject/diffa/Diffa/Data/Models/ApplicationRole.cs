﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models
{
    public class ApplicationRole: IdentityRole<Guid>
    {
        public bool IsStaff { get; set; }
    }
}
