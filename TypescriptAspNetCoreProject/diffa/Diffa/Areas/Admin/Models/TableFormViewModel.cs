﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin.Models
{
    public class TableFormViewModel
    {
        public Guid TableId { get; set; }
        public string TableName { get; set; }
        public string HexColor { get; set; }
        public int TablesAvailable { get; set; }
        public int SeatsPerTable { get; set; }
        public double PricePerSeat { get; set; }
        public Guid? EventId { get; set; }
        public List<SelectListItem> Events { get; set; } = new List<SelectListItem>();
    }
}
