﻿using Diffa.Areas.Admin.Helpers;
using Diffa.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin
{
    [Route("admin/products")]
    [Area("Admin")]
    public class ProductsController : Controller
    {
        ProductsHelper _helper;
        List<SelectListItem> _selectableEventList;
        public ProductsController(ProductsHelper helper, EventsHelper eventsHelper)
        {
            _helper = helper;
            _selectableEventList = GetSelectableEvents(eventsHelper);
        }

        private List<SelectListItem> GetSelectableEvents(EventsHelper eventsHelper)
        {
            var events = eventsHelper.GetEvents();
            var selectedList = new List<SelectListItem>();
            selectedList.Add(new SelectListItem()
            {
                Value = null,
                Text = string.Empty
            });

            foreach(var e in events)
            {
                var item = new SelectListItem()
                {
                    Value = e.EventId.ToString(),
                    Text = e.Name
                };
                selectedList.Add(item);
            }

            return selectedList;
        }

        [Route("")]
        [Route("Index")]
        [Authorize(Roles = "Administrator")]
        public ActionResult Index(Guid eventId)
        {
            var products = _helper.GetProducts(eventId);
            return View(products);
        }

        [Route("add-product")]
        [Authorize(Roles = "Administrator")]
        public ActionResult AddProduct()
        {
            var model = new ProductFormViewModel();
            model.Events = _selectableEventList;
            return View(model);
        }

        [Route("add-product")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddProduct(ProductFormViewModel model)
        {
            var success = _helper.AddProduct(model);
            if (success)
                return new RedirectResult("/admin/manage-products");

            return View(model);
        }

        [Route("edit-product")]
        [Authorize(Roles = "Administrator")]
        public ActionResult EditProduct(Guid productId)
        {
            var model = _helper.GetProduct(productId);
            model.Events = _selectableEventList;
            return View(model);
        }

        [Route("edit-product")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult EditProduct(ProductFormViewModel model)
        {
            var success = _helper.EditProduct(model);
            if (success)
                return new RedirectResult("/admin/manage-products");

            return View(model);
        }

        [Route("delete-product")]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteProduct(Guid productId)
        {
            _helper.DeleteProduct(productId);
            return new RedirectResult("/admin/manage-products");
        }
    }
}
