﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Global
{
    public class AjaxResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
