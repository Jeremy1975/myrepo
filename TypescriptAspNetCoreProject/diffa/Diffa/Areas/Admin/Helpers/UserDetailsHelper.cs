﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Global;
using Diffa.Data.Models;
using Diffa.Data.Transformed;
using Diffa.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin.Helpers
{
    public class UserDetailsHelper
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IEmailTemplatedSender emailSender;

        public UserDetailsHelper (UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager,
            IEmailTemplatedSender emailSender)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.emailSender = emailSender;
        }
        
        async internal Task<StaffDetailsViewModel> GetStaffDetailsViewModel(int take=50, int skip=0)
        {
            StaffDetailsViewModel model = new StaffDetailsViewModel();
            model.Users = this.userManager.Users.Where(p => p.IsStaff).Skip(skip).Take(take).Select(p => new Data.Transformed.ApplicationUserWithRoles(p)).ToList();
            model.Roles = this.roleManager.Roles.Where(p=>p.IsStaff).ToList();
            foreach(var user in model.Users)
            {
                var roles = await this.userManager.GetRolesAsync(user as ApplicationUser);
                user.Roles = roles.ToList();
            }
            return model;
        }

        internal StaffFormViewModel GetStaffViewModel()
        {
            StaffFormViewModel model = new StaffFormViewModel();
            //requirements change, all users are admin and admin is only role (selected = true)
            model.Roles= this.roleManager.Roles.Where(p => p.IsStaff).Select(p=>new SelectableRoles(p) { Selected=true }).ToList();
            return model;
        }

        internal AjaxResult EditStaffMember(StaffFormViewModel model)
        {
            var existingUser = userManager.FindByIdAsync(model.Id.ToString()).Result;

            if (existingUser != null)
            {
                existingUser.UserName = model.Email;
                existingUser.Email = model.Email;
                existingUser.FirstName = model.FirstName;
                existingUser.LastName = model.LastName;
                existingUser.PhoneNumber = model.PhoneNumber;
                
                IdentityResult result = userManager.UpdateAsync(existingUser).Result;

                if (result.Succeeded)
                {
                    //do roles
                    return new AjaxResult { Success = true };
                }
            }
            return new AjaxResult { Success = false, Message = "User Not Found" };
        }

        async internal Task<AjaxResult> DeleteStaff(Guid userId)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            if (user != null)
            {
                IdentityResult result = await userManager.DeleteAsync(user);
                if (result.Succeeded)
                    return new AjaxResult { Success = true };
               
            }
            return new AjaxResult { Success = false };
        }

        async internal Task<StaffFormViewModel> GetStaffViewModel(Guid userId)
        {
            var viewModel = this.GetStaffViewModel();
            var user = this.userManager.Users.Where(p => p.Id == userId).FirstOrDefault();
            var selectedRoles = (await this.userManager.GetRolesAsync(user)).ToList();

            viewModel.Roles = this.roleManager.Roles.Where(p => p.IsStaff).Select(p=>new SelectableRoles(p)).ToList();
            viewModel.Email = user.Email;
            viewModel.FirstName = user.FirstName;
            viewModel.LastName = user.LastName;
            viewModel.PhoneNumber = user.PhoneNumber;
            viewModel.Id = user.Id;

            foreach(var role in selectedRoles)
            {
                if (viewModel.Roles.Where(p => p.Name == role).Any())
                {
                    viewModel.Roles.Where(p => p.Name == role).First().Selected = true;
                }

            }

            return viewModel;


        }

        internal AjaxResult AddStaffMember(StaffFormViewModel model, Microsoft.AspNetCore.Mvc.IUrlHelper url)
        {
            if (userManager.FindByNameAsync(model.Email).Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = model.Email;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;
                user.IsStaff = true;
                IdentityResult result = userManager.CreateAsync(user, "A&" + Guid.NewGuid().ToString()).Result;

                if (result.Succeeded)
                {
                    foreach(var role in model.Roles.Where(p => p.Selected))
                    {
                        userManager.AddToRoleAsync(user, role.Name).Wait();
                    }
                    var code = this.userManager.GenerateEmailConfirmationTokenAsync(user).Result;
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    
                    var callbackUrl = url.PageLink("/Account/ConfirmEmail",null,new { area = "Identity", userId = user.Id, code = code});
                    var dict = new Dictionary<string, string>();
                    dict.Add("ConfirmUrl", HtmlEncoder.Default.Encode(callbackUrl));
                    
                    //,$"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>."
                    this.emailSender.SendEmailAsync(model.Email, "Confirm your email",this.emailSender.MergeTemplate("Diffa_Master", "EmailConfirmation", dict)).Wait();
                    return new AjaxResult { Success = true };
                }
            }
            return new AjaxResult { Success = false, Message="Email already in use" };
        }

        internal object GetCustomerDetailsViewModel()
        {
            CustomerDetailsViewModel model = new CustomerDetailsViewModel();
            model.Users = this.userManager.Users.Where(p => !p.IsStaff).ToList();
            return model;
        }
    }
}
