﻿using Diffa.Areas.Home.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Home
{
    [Area("Home")]
    [Route("")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly HomeHelper helper;
        public HomeController(ILogger<HomeController> logger, HomeHelper helper)
        {
            _logger = logger;
            this.helper = helper;
            
        }

        [Route("")]
        async public Task<IActionResult> Index()
        {
            var model=await this.helper.GetHomeModel(this.HttpContext.User);
            return View(model);
        }

        [Route("login-register")]
        public IActionResult LoginRegister()
        {
            return View();
        }

    }
}
