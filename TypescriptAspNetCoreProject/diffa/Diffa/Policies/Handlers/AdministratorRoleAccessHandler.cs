﻿using Diffa.Policies.Requirements;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Policies.Handlers
{
    public class AdministratorRoleAccessHandler : AuthorizationHandler<AdminstratorRoleAccessRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminstratorRoleAccessRequirement requirement)
        {
            foreach(var role in context.User.Claims.Where(p=>p.Type== PolicyNames.AdminstratorRoleAccessClaim))
            {
                if (requirement.ModuleName == role.Value)
                {
                    context.Succeed(requirement);
                    return Task.CompletedTask;
                }
            }
            return Task.CompletedTask;
        }
    }
}
