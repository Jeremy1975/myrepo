﻿class AdminManageEvents {
    promoDateValues: Map<string,Date>

    constructor() {
        this.promoDateValues = new Map<string, Date>();
    }
    LoadEvents() {
        $("#liEvents").addClass("active");
        $("#EventList").load("/admin/events/events", () => {
            $("#js-add-event").on("click", (e) => this.LoadAddEvent(e));
            $(".js-edit-event").on("click", (e) => this.LoadEditEvent(e));
            $(".js-delete-event").on("click", (e) => this.DeleteEvent(e));
            $(".js-tables-event").on("click", (e) => this.TablesEvent(e));
            $(".js-products-event").on("click", (e) => this.ProductsEvent(e));
            $(".js-publish-event").on("click", (e) => this.PublishEvent(e));
        });
    }
    PublishEvent(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        if (confirm("Are you sure you want to publish this event?")) {
            window.location.href = "/admin/events/publish-event?eventId=" + $(e.currentTarget).data("id")
        }
    }

    TablesEvent(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#main-container").hide();
        window.location.href = "/admin/events/tables-event?eventId=" + $(e.currentTarget).data("id")
    }

    ProductsEvent(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#main-container").hide();
        window.location.href = "/admin/events/products-event?eventId=" + $(e.currentTarget).data("id")
    }

    DeleteEvent(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        if (confirm("Are you sure you want to delete this event?"))
            window.location.href = "/admin/events/delete-event?eventId=" + $(e.currentTarget).data("id")
    }

    LoadAddEvent(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#main-container").hide();
        $("#modal-container").load("/admin/events/add-event", () => {
            this.BindEventForm();
        });
    }

    LoadEditEvent(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        $("#main-container").hide();
        $("#modal-container").load("/admin/events/edit-event?eventId=" + $(e.currentTarget).data("id"), () => {
            this.BindEventForm();
        });
    }

    BindEventForm() {
        $.validator.unobtrusive.parse("#js-event-form");
        $("#js-cancel").on("click", () => { this.CancelModal() });
        $("#ZipCode").mask('00000-0000');
        $("#AllowInstallmentPlan").on("click", () => $("#divLastPaymentDate").toggle());
        $("#btnDiscountCodes").on("click", (e) => this.AddDiscountCodes(e));
        if ($("#AllowInstallmentPlan:checked").length > 0)
            $("#divLastPaymentDate").toggle();

        $("#js-cancel-promo").on("click", (e) => this.CancelDiscountCodes(e));
        $("#js-save-promo").on("click", (e) => this.SaveDiscountCodes(e));
        $("#promoCodeGrid").kendoGrid({
            columns: [
                { field: "name" },
                { field: "price", width: 200 },
                { field: "date", editable: () => { return false } },
                { field: "flat", editable: () => { return false }, width:100 },
                { command: "destroy",width: 200  }
            ],
            editable: {
                mode: "incell"
            },
            remove: () => {
                this.RemoveRow();
            }
        });

        $("input.js-discount-date").kendoDateTimePicker({
            change: (e) => {
                var existingId = e.sender.element.parents("tr").first().find("input.js-existing-code-hidden").data("existing-id");
                this.promoDateValues.set(existingId, e.sender.value());
            }
        });

        $("#js-add-promo-code").on("click", () => this.AddPromoRow());

        $.validator.addMethod("greaterThanEventStart", function (value, element) {
            var startDate = new Date($("#EventStart").val().toString());
            if (new Date(value) > startDate) {
                return true;
            }

            return false;
            
        }, "Must be after event start date");

    }
    SaveDiscountCodes(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        e.preventDefault();
        this.CancelDiscountCodes(e);
    }
    RemoveRow() {
        window.setTimeout(()=> {
            $("input.js-discount-date").each((index, element) => this.SetDiscountDates(index, element));
        }, 100);
    }
    AddPromoRow(): any {
        $("#promoCodeGrid").data("kendoGrid").addRow();
        $($("#promoCodeGrid td").get(2)).html("<input class='js-discount-date' />");
        $("input.js-discount-date").each((index, element) => this.SetDiscountDates(index, element));
        
    }
    SetDiscountDates(index: number, element: HTMLElement): false | void {
        var id = $(element).parents("tr").first().find("input.js-existing-code-hidden").data("existing-id");
        $(element).kendoDateTimePicker({
            value: this.promoDateValues.get(id),
            change: (e) => {
                var existingId = e.sender.element.parents("tr").first().find("input.js-existing-code-hidden").data("existing-id");
                this.promoDateValues.set(existingId, e.sender.value());
            }
        });

    }
    CancelDiscountCodes(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        e.preventDefault();
        $("#EventContainer").show();
        $("#PromoContainer").hide();
    }

    AddDiscountCodes(e: JQuery.ClickEvent<HTMLElement, undefined, HTMLElement, HTMLElement>): any {
        e.preventDefault();
        $("#EventContainer").hide();
        $("#PromoContainer").show();

    }

    CancelModal() {
        $("#modal-container").html("");
        $("#main-container").show();
    };
}