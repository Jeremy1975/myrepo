﻿using Diffa.Data;
using Diffa.Data.Repos;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Logging
{
    public class LoggerDatabaseProvider : ILoggerProvider
    {
        IApplicationBuilder app;
        private ILoggingRepo repo;

        public LoggerDatabaseProvider(IApplicationBuilder app)
        {
            this.app = app;
        }

        public ILogger CreateLogger(string categoryName)
        {
            this.repo = app.ApplicationServices.CreateScope().ServiceProvider.GetService(typeof(ILoggingRepo)) as ILoggingRepo;
            return new DbLogger(categoryName,repo);
        }

        public void Dispose()
        {
            
        }
    }
}
