<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Client;
use App\Models\ClientOrder;
use App\Models\TransporterOrder;
use DB;
use DateTime;

class OrderController extends Controller
{
    public function index()
    {
        return view('order/index');
    }

    public function getAllOrders()
    {
        $orders = DB::select('SELECT os.id, os.date_created, os.remarks, os.client_id, os.account_no AS cno, os.name AS cname,
        tt.transporter_id, tt.account_no AS tno, tt.name AS tname FROM
        (SELECT o.id, o.date_created, o.cancelled, o.remarks, co.order_id, co.client_id, c.account_no, c.name
        FROM orders AS o JOIN clients_orders AS co ON o.id = co.order_id JOIN clients AS c ON c.id = co.client_id) AS os
        LEFT JOIN (SELECT ot.order_id, ot.transporter_id, t.account_no, t.name FROM transporters_orders AS ot
        JOIN transporters AS t ON ot.transporter_id = t.id) AS tt ON os.id = tt.order_id WHERE os.cancelled = 0 ORDER BY os.id desc');

        return view('order/all', ['orders' => $orders]);
    }

    public function getOrder($id)
    {
        $order = Order::where('id', $id)->first();
        $clients = ClientOrder::where('order_id', $id)->join('clients', 'clients.id', '=', 'clients_orders.client_id')->get();
        $transporters = TransporterOrder::where('order_id', $id)->join('transporters', 'transporters.id', '=', 'transporters_orders.transporter_id')->get();
        return view('order/detail', ['order' => $order, 'clients' => $clients, 'transporters' => $transporters]);
    }

    public function save(Request $request)
    {
        $remarks = $request->remarks;
        $created_date = $request->created_date;
        $select_client_id = $request->select_client_id;

        $result = 0;
        try{
            $new_order = Order::create(['branch' => 'J', 'date_created' =>  new DateTime($created_date), 'remarks' => $remarks]);
            $new_client_order = ClientOrder::create(['order_id' => $new_order->id, 'client_id' => $select_client_id]);

            if($new_client_order){
                $result = $new_order->id;
            }
        }
        catch(QueryException $e){
            $result = 0;
        }
        catch(Exception $e){
            $result = -1;
        }

        return response()->json(['success' => $result]);
    }

    public function getTransporterOrder(Request $request)
    {
        $result = 0;
        $order_id = $request->order_id;
        $transporter_id = $request->transporter_id;
        $existing_record = TransporterOrder::select('*')->where('order_id', $order_id)->where('transporter_id', $transporter_id)->first();

        if(!empty($existing_record)){
            $result = $existing_record->id;
        }

        return response()->json(['success' => $result]);
    }
}
