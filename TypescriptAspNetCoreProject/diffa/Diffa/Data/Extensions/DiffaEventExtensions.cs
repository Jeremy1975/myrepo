﻿using Diffa.Areas.Admin.Models;
using Diffa.Data.Models.DiffaEvents;
using System;

namespace Diffa.Data.Extensions
{
    public static class DiffaEventExtensions
    {
        public static EventFormViewModel ToEventFormViewModel(this DiffaEvent diffaEvent)
        {
            return new EventFormViewModel
            {
                Name=diffaEvent.Name,
                EventId=diffaEvent.EventId,
                 AllowInstallmentPlan=diffaEvent.AllowInstallmentPlan,
                 BookingStartDate=diffaEvent.BookingStartDate,
                 City=diffaEvent.City,
                 Description=diffaEvent.Description,
                  DiscountCodes=diffaEvent.DiscountCodes,
                 EventEnd=diffaEvent.EventEnd,
                 //EventLogo=diffaEvent.LogoPath,
                 EventStart=diffaEvent.EventStart,
                 EventType=diffaEvent.EventType,
                 LastPaymentDate=diffaEvent.LastPaymentDate,
                 LogoPath = diffaEvent.LogoPath,
                 PublishedDate=diffaEvent.PublishedDate,
                 State=diffaEvent.State,
                 StreetAddress=diffaEvent.StreetAddress,
                 //TableLayout=diffaEvent.ta
                 TotalTickets=diffaEvent.TotalTickets,
                 ZipCode=diffaEvent.ZipCode
            };
        }

        public static DiffaEvent ToDiffaEvent(this EventFormViewModel eventForm)
        {
            return new DiffaEvent
            {
                Name = eventForm.Name,
                EventId=eventForm.EventId,
                AllowInstallmentPlan = eventForm.AllowInstallmentPlan,
                BookingStartDate = eventForm.BookingStartDate.HasValue ? eventForm.BookingStartDate.Value : DateTime.Now,
                City = eventForm.City,
                Description = eventForm.Description,
                DiscountCodes = eventForm.DiscountCodes,
                EventEnd = eventForm.EventEnd.HasValue ? eventForm.EventEnd.Value : DateTime.Now,
                //EventLogo=eventForm.LogoPath,
                EventStart = eventForm.EventStart.HasValue ? eventForm.EventStart.Value : DateTime.Now,
                EventType = eventForm.EventType,
                LastPaymentDate = eventForm.LastPaymentDate,
                LogoPath = eventForm.LogoPath,
                PublishedDate = eventForm.PublishedDate,
                State = eventForm.State,
                StreetAddress = eventForm.StreetAddress,
                //TableLayout=eventForm.ta
                TotalTickets = eventForm.TotalTickets,
                ZipCode = eventForm.ZipCode
            };
        }

        public static DiffaEvent ToEdited(this DiffaEvent eventForm, EventFormViewModel changes)
        {
            eventForm.Name = changes.Name;
            eventForm.AllowInstallmentPlan = changes.AllowInstallmentPlan;
            eventForm.BookingStartDate = changes.BookingStartDate.HasValue ? changes.BookingStartDate.Value : eventForm.BookingStartDate;
            eventForm.City = changes.City;
            eventForm.Description = changes.Description;
            eventForm.EventEnd = changes.EventEnd.HasValue ? changes.EventEnd.Value : eventForm.EventEnd;
            eventForm.EventStart = changes.EventStart.HasValue ? changes.EventStart.Value : eventForm.EventStart;
            eventForm.EventType = changes.EventType;
            eventForm.LastPaymentDate = changes.LastPaymentDate;
            eventForm.PublishedDate = changes.PublishedDate;
            eventForm.State = changes.State;
            eventForm.StreetAddress = changes.StreetAddress;
            eventForm.TotalTickets = changes.TotalTickets;
            eventForm.ZipCode = changes.ZipCode;
            return eventForm;
        }
    }
}
