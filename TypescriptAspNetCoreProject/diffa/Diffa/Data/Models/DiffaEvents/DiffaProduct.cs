﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.DiffaEvents
{
    public class DiffaProduct
    {
        [Key]
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public double ProductPrice{ get; set; }
        public string LogoPath { get; set; }
        public DiffaEvent DiffaEvent { get; set; }
        public bool IsDeleted{ get; set; }
        public Guid? DiffaEventEventId { get; set; }
    }
}
