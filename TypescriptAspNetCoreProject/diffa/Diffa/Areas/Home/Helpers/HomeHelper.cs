﻿using Diffa.Areas.Home.Models;
using Diffa.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Home.Helpers
{
    public class HomeHelper
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        public HomeHelper(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        async internal Task<HomeModel> GetHomeModel(System.Security.Claims.ClaimsPrincipal user)
        {
            HomeModel model = new HomeModel();
            model.CurrentUser = await this.userManager.GetUserAsync(user);
            return model;
        }
    }
}
