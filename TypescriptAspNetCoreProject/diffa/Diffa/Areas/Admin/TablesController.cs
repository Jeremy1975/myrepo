﻿using Diffa.Areas.Admin.Helpers;
using Diffa.Areas.Admin.Models;
using Diffa.Data.Global;
using Diffa.Data.Models.DiffaEvents;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace Diffa.Areas.Admin
{
    [Route("admin/tables")]
    [Area("Admin")]
    public class TablesController : Controller
    {
        private TablesHelper _helper;
        List<SelectListItem> _selectableEventList;

        public TablesController(TablesHelper helper, EventsHelper eventsHelper)
        {
            _helper = helper;
            _selectableEventList = GetSelectableEvents(eventsHelper);
        }

        private List<SelectListItem> GetSelectableEvents(EventsHelper eventsHelper)
        {
            var events = eventsHelper.GetEvents();
            var selectedList = new List<SelectListItem>();
            selectedList.Add(new SelectListItem()
            {
                Value = null,
                Text = string.Empty
            });

            foreach (var e in events)
            {
                var item = new SelectListItem()
                {
                    Value = e.EventId.ToString(),
                    Text = e.Name
                };
                selectedList.Add(item);
            }

            return selectedList;
        }

        [Route("")]
        [Route("Index")]
        [Authorize(Roles = "Administrator")]
        public ActionResult Index(Guid eventId)
        {
            var list = _helper.GetEventTables(eventId);
            return View(list);
        }

        [Route("add-table")]
        [Authorize(Roles = "Administrator")]
        public ActionResult AddTable()
        {
            var model = new TableFormViewModel();
            model.Events = _selectableEventList;
            return View(model);
        }

        [Route("add-table")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddTable(TableFormViewModel model)
        {
            var success = _helper.AddEventTable(model);
            if (success)
                return new RedirectResult("/admin/manage-tables");

            return View(model);
        }

        [Route("edit-table")]
        [Authorize(Roles = "Administrator")]
        public ActionResult EditTable(Guid tableId)
        {
            var model = _helper.GetEventTable(tableId);
            model.Events = _selectableEventList;
            return View(model);
        }

        [Route("edit-table")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult EditTable(TableFormViewModel model)
        {
            var success = _helper.EditEventTable(model);
            if (success)
                return new RedirectResult("/admin/manage-tables");

            return View(model);
        }

        [Route("table-layout")]
        [Authorize(Roles = "Administrator")]
        public ActionResult Layout(Guid eventId)
        {
            var list = _helper.GetEventTables(eventId);
            var positions = _helper.GetTablePositions(eventId);
            var elementPositions = _helper.GetElementPositions(eventId);
            return View(new TableLayoutViewModel { Tables = list, Positions = positions, ElementPositions = elementPositions, EventId = eventId });
        }

        [Route("table-layout-set")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult SetLayout(Guid tableId, int tableIndex, double top, double left)
        {
            AjaxResult result = _helper.SetLayout(tableId, tableIndex, top, left);
            return Json(top);
        }

        [Route("table-layout-element-set")]
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult SetElementLayout(Guid eventId, string elementName, double top, double left)
        {
            AjaxResult result = _helper.SetLayout(eventId, elementName, top, left);
            return Json(top);
        }
    }
}
