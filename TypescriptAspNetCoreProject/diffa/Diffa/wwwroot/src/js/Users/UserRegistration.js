/// <reference types="jquery" />
var UserRegistration = /** @class */ (function () {
    function UserRegistration() {
        this.instance = null;
        this.phoneNumber = "";
        this.instance = this;
    }
    UserRegistration.prototype.init = function () {
        var _this = this;
        $("#Input_PhoneNumber").mask('000-000-0000');
        $("#Input_PhoneNumber").on("keyup", function (e) {
            _this.phoneNumber = $(e.target).val().toString();
            console.log(_this.phoneNumber);
        });
    };
    return UserRegistration;
}());
var userRegistration = new UserRegistration();
userRegistration.init();
//# sourceMappingURL=UserRegistration.js.map