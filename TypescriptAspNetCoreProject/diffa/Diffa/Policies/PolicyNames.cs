﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Policies
{
    public static class PolicyNames
    {
        public const string StaffManagement = "StaffManagement";
        public const string UserManagement = "UserManagement";
        public const string EventManagement = "EventManagement";
        public const string TableManagement = "TableManagement";
        public const string RightsManagement = "RightsManagement";
        public const string SponsorManagement = "SponsorManagement";

        public const string AdminstratorRoleAccessClaim = "AdminstratorRoleAccessClaim";

        public static List<string> GetAllModuleNames()
        {
            return new List<string>
            {
                StaffManagement,
                UserManagement,
                EventManagement,
                TableManagement,
                RightsManagement,
                SponsorManagement
            };
        }
    }
}
