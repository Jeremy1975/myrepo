<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransporterOrderDetail extends Model
{
    protected $table = 'transporter_order_details';
    protected $fillable = [
        'transporter_order_id', 'gi_att', 'gi_desc', 'gi_tons', 'gi_abnormal', 'gi_reqd', 'gi_value', 'gi_currency', 'gi_rate', 'gi_terms', 'gi_instruction',
        'v_type_id', 'v_l', 'v_w', 'v_h', 'v_add_dimension', 'load_addr_id', 'offload_addr_id'
    ];
}
