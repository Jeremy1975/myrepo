﻿using Diffa.Data;
using Diffa.Data.Models;
using Diffa.Data.Repos;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Logging
{
    public class DbLogger : ILogger
    {
        
        private readonly ILoggingRepo repo;
        private readonly string categoryName;
        public DbLogger(string categoryName,ILoggingRepo repo)
        {
            this.repo = repo;
            this.categoryName = categoryName;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (logLevel == LogLevel.Critical || logLevel == LogLevel.Error || logLevel == LogLevel.Warning)
                RecordMsg(logLevel, eventId, state, exception, formatter);

            if(logLevel==LogLevel.Information)
                RecordInfo(state, exception, formatter);
        }

        private void RecordMsg<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {

            repo.LogError(new ErrorLog
            {
                LogLevel = logLevel.ToString(),
                CategoryName = categoryName,
                Msg = formatter(state, exception),
                Timestamp = DateTime.UtcNow
            });

        }

        private void RecordInfo<TState>(TState state, Exception exception, Func<TState, Exception, string> formatter)
        {

            repo.LogInfo(new InformationLog
            {
                CategoryName = categoryName,
                Msg = formatter(state, exception),
                Timestamp = DateTime.UtcNow
            });
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return new NoopDisposable();
        }

        private class NoopDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }
    }
}