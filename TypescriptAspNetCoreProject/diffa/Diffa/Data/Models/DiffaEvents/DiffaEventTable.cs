﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.DiffaEvents
{
    public class DiffaEventTable
    {
        [Key]
        public Guid TableId { get; set; }
        public string TableName{ get; set; }
        public string HexColor { get; set; }
        public int TablesAvailable { get; set; }
        public int SeatsPerTable { get; set; }
        public double PricePerSeat { get; set; }
        public DiffaEvent DiffaEvent { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? DiffaEventEventId { get; set; }
    }
}
