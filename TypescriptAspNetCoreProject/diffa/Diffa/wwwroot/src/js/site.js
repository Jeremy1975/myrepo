﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function windowheight() {
    var windowheight = $(window).outerHeight(); // Get Window Height
    //var headerheight = $(".inner-header").outerHeight(); // Get Header Height
    //var totheight = windowheight - headerheight; // Total of Window Height & Header Height
    $('.body-wrapper').css('min-height', windowheight); //Apply Min Height to Page Wrapper
}
// Profile Toggle
$(document).ready(function () {
    $('.user-profile-outer').click(function (event) {
        event.stopPropagation();
        $(".logout").slideToggle("fast");
        $(".user-profile-outer").toggleClass("active");
    });
    $(".user-profile-outer .logout").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $(".logout").hide();
    $(".user-profile-outer").removeClass("active");
});

//Function Call on Window Resize
$(window).resize(function () {
    windowheight();
});
$(window).on("load", function () {
    windowheight();
});

////Page Loader
//$(window).on("load", function () {
//    $(".pre_loader").fadeOut("slow");
//});
