﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models.Cart
{
    public class ShoppingCart
    {
        [Key]
        public Guid ShoppingCartId { get; set; }
        public Guid UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public List<ShoppingCartDetail> ShoppingCartDetails { get; set; }
        public string OrderNumber { get; set; }
    }
}
