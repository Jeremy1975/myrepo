<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'AuthController@index');
Route::post('auth/login', 'AuthController@login');
Route::get('dashboard', 'DashboardController@index');
Route::get('auth/logout', 'AuthController@logout');
Route::get('user/admin', 'UserController@getManagers');
Route::get('user/customer', 'UserController@getCustomers');
Route::post('user/newCustomer', 'UserController@createCustomer');
Route::post('user/editCustomer', 'UserController@editCustomer');
Route::post('user/removeCustomer', 'UserController@removeCustomer');

Route::get('order/all', 'OrderController@getAllOrders');
Route::post('order/save', 'OrderController@save');
Route::get('order/detail/{id}', 'OrderController@getOrder');
Route::post('order/getTransporterOrder', 'OrderController@getTransporterOrder');

Route::get('client/all', 'ClientController@getAllClients');
Route::post('client/add', 'ClientController@add');
Route::post('client/create', 'ClientController@create');
Route::post('client/edit', 'ClientController@edit');
Route::post('client/delete', 'ClientController@delete');
Route::get('client/list', 'ClientController@getClientList');
Route::post('client/remove', 'ClientController@remove');

Route::get('transporter/all', 'TransporterController@getAllTransporters');
Route::post('transporter/add', 'TransporterController@add');
Route::post('transporter/create', 'TransporterController@create');
Route::post('transporter/edit', 'TransporterController@edit');
Route::post('transporter/delete', 'TransporterController@delete');
Route::get('transporter/detail/{id}', 'TransporterController@getOrderTransporter');
Route::get('transporter/list', 'TransporterController@getTransporterList');
Route::post('transporter/remove', 'TransporterController@remove');
Route::post('transporter/saveConfirmData', 'TransporterController@saveConfirmationData');
Route::get('transporter/pdf/{id}', 'TransporterController@generatePDF');
Route::post('transporter/change', 'TransporterController@change');

Route::post('vehicleType/create', 'VehicleTypeController@create');
Route::get('vehicleType/all', 'VehicleTypeController@getAllVehicleTypes');

Route::get('loadAddrs', 'LoadAddressController@index');
Route::post('loadAddrs/create', 'LoadAddressController@create');
Route::post('loadAddrs/edit', 'LoadAddressController@edit');
Route::post('loadAddrs/remove', 'LoadAddressController@remove');

Route::get('offloadAddrs', 'OffLoadAddressController@index');
Route::post('offloadAddrs/create', 'OffLoadAddressController@create');
Route::post('offloadAddrs/edit', 'OffLoadAddressController@edit');
Route::post('offloadAddrs/remove', 'OffLoadAddressController@remove');


