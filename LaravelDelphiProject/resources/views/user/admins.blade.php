@push('styles')
    <link href="{{asset('plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
@endpush
@extends('layouts.master')
@section('title', 'Administrator')
@section('content')
<div class="row">
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <table id="admins" class="display responsive-table">
                    <thead>
                        <tr>
                            <th style="display:none">ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Authorization</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="display:none">ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Authorization</th>
                            <th>#</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($admins as $admin)
                            <tr>
                                <td style="display:none">{{$admin->id}}</td>
                                <td>{{$admin->name}}</td>
                                <td>{{$admin->email}}</td>
                                <td>{{$admin->level}}</td>
                                <td>
                                    <a href="#edit_admin_modal" data-perm-item="{{$admin}}" onclick="showEditModal(this)" class="modal-trigger waves-effect waves-light btn">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="edit_admin_modal" class="modal modal-footer">
    <form class="col s12">
        <div class="modal-content">
            <h4>Edit User</h4>
            <div class="row">
                <input id="edit_user_id" type="hidden">
                <div class="input-field col s12">
                    <input id="edit_name" name="edit_name" type="text" class="validate" placeholder="">
                    <label for="edit_name">Name</label>
                </div>
                <div class="input-field col s12">
                    <input id="edit_email" name="edit_email" type="text" class="validate" placeholder="">
                    <label for="edit_email">Email</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" onclick="editAdministrator()">Edit</a>
        </div>
    </form>
</div>

@push('javascript')
<script>
function showEditModal(obj)
{
    var item = $(obj).data('perm-item');
    $('#edit_user_id').val(item['id']);
    $('#edit_name').val(item['name']);
    $('#edit_email').val(item['email']);
}
</script>
@endpush

@push('javascript')
    <script src="{{asset('plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/tables/users.js')}}"></script>
    <script src="{{asset('plugins/google-code-prettify/prettify.js')}}"></script>
    <script src="{{asset('js/pages/ui-modals.js')}}"></script>
@endpush
@endsection
