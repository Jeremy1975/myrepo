﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models
{
    public class ApplicationModule
    {
        public Guid Id { get; set; }
        public string ModuleName { get; set; }
    }
}
