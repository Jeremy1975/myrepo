﻿class AdminManageLayouts {
    selectedTableId: string
    selectedTableIndex: string
    eventId: string

    constructor(eventId: string) {
        this.eventId = eventId;
        console.log(eventId);
    }

    Init() {
        this.SetupDraggables();
        $(".addMe").on("click", (e) => {
            $(".draggable-selected").css("background-color", $(e.target).css("color"));
        });
        
    }


    SetupDraggables() {
        $(".draggable,.draggable-element").each((index, element) => {
            $(element).offset({ top: $(element).data("initialOffsetTop"), left: $(element).data("initialOffsetLeft") });
        }
        );

        $('.draggable').on("click", (e) => {
            $(".draggable-selected").removeClass("draggable-selected");
            $(e.target).addClass("draggable-selected");
            this.selectedTableId = $(e.target).data("tableId");
            this.selectedTableIndex = $(e.target).data("tableIndex");
            $("#Tables").show();
        });

        $('.draggable').kendoDraggable({
            hint: function (original) {
                return original.clone().addClass("drag-clone");
            },
            dragstart: function (e) {
                if (!$(e.originalEvent.target).hasClass("draggable"))
                    return;

                $(e.originalEvent.target).addClass("drag-hide");
            }
        });


        $('.draggable-element').kendoDraggable({
            hint: function (original) {
                return original.clone().addClass("drag-clone");
            },
            dragstart: function (e) {
                if (!$(e.originalEvent.target).hasClass("draggable-element"))
                    return;

                $(e.originalEvent.target).addClass("drag-hide");
            }
        });

        $('body').kendoDropTarget({
            drop: (e)=> {

                var pos = $(".drag-clone").offset();
                $(e.draggable.currentTarget)
                    .removeClass("drag-hide")
                    .offset(pos);

                var url = "";
                var data = {};
                if (e.draggable.currentTarget.data("tableId") == undefined) {
                    url = "/admin/tables/table-layout-element-set";
                    data = {
                        eventId: this.eventId,
                        elementName: e.draggable.currentTarget.attr("id"),
                        top: pos.top,
                        left: pos.left
                    }
                }

                else {
                    url = "/admin/tables/table-layout-set";
                    data = {
                        tableId: e.draggable.currentTarget.data("tableId"),
                        tableIndex: e.draggable.currentTarget.data("tableIndex"),
                        top: pos.top,
                        left: pos.left
                    }
                }

                $.ajax(
                    {
                        type: "POST",
                        url: url,
                        data: data
                    });
            }
        })
    }
}