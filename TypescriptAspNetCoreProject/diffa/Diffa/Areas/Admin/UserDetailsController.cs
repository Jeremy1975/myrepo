﻿using Diffa.Areas.Admin.Helpers;
using Diffa.Areas.Admin.Models;
using Diffa.Data.Global;
using Diffa.Data.Models;
using Diffa.Policies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Areas.Admin
{
    [Area("Admin")]
    [Route("admin/user-detail")]
    [Authorize(Policy = PolicyNames.UserManagement)]
    public class UserDetailsController: Controller
    {
        private readonly UserDetailsHelper userDetailsHelper;

        public UserDetailsController(UserDetailsHelper helper)
        {
            this.userDetailsHelper = helper;
        }

        [Route("staff-details")]
        async public Task<ActionResult> StaffDetails()
        {
            return View(await this.userDetailsHelper.GetStaffDetailsViewModel());
        }

        [Route("customer-details")]
        public ActionResult CustomerDetails()
        {
            return View(this.userDetailsHelper.GetCustomerDetailsViewModel());
        }

        [Route("add-staff")]
        [HttpGet]
        public ActionResult AddStaff()
        {

            return View(this.userDetailsHelper.GetStaffViewModel());
        }

        [Route("add-staff")]
        [HttpPost]
        public JsonResult AddStaff(StaffFormViewModel model)
        {
            AjaxResult res = this.userDetailsHelper.AddStaffMember(model, this.Url);
            return Json(res);
        }

        [Route("edit-staff/{userId}")]
        [HttpGet]
        public ActionResult EditStaff(Guid userId)
        {
            return View(this.userDetailsHelper.GetStaffViewModel(userId).Result);
        }

        [Route("edit-staff")]
        [HttpPost]
        public ActionResult EditStaff(StaffFormViewModel model)
        {
            AjaxResult res = this.userDetailsHelper.EditStaffMember(model);
            return Json(res);
        }

        [Route("delete-staff")]
        [HttpPost]
        async public Task<JsonResult> DeleteStaff(Guid userId)
        {
            AjaxResult res = await this.userDetailsHelper.DeleteStaff(userId);
            return Json(res);
        }
    }
}
