﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diffa.Data.Models
{
    public class RoleModule
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public Guid ModuleId { get; set; }
        public bool Disabled { get; set; }

    }
}
