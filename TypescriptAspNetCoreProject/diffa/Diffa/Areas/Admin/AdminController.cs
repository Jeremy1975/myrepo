﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Diffa.Areas.Admin
{
    [Route("Admin")]
    [Area("Admin")]
    public class AdminController: Controller
    {
        [Route("")]
        [Route("Index")]
        [Authorize(Roles ="Administrator")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("manage-events")]
        [Authorize(Roles = "Administrator")]
        public ActionResult ManageEvents()
        {
            return View();
        }

        [Route("manage-products")]
        [Authorize(Roles = "Administrator")]
        public ActionResult ManageProducts(Guid? eventId)
        {
            ViewBag.EventId = eventId;
            return View();
        }

        [Route("manage-payments")]
        [Authorize(Roles = "Administrator")]
        public ActionResult ManagePayments()
        {
            return View();
        }

        [Route("manage-tables")]
        [Authorize(Roles = "Administrator")]
        public ActionResult ManageTables(Guid? eventId)
        {
            ViewBag.EventId = eventId;
            return View();
        }

        [Route("manage-users")]
        [Authorize(Roles = "Administrator")]
        public ActionResult ManageUsers()
        {
            return View();
        }

        [Route("general-reports")]
        [Authorize(Roles = "Administrator")]
        public ActionResult GeneralReports()
        {
            return View();
        }

        [Route("notifications")]
        [Authorize(Roles = "Administrator")]
        public ActionResult Notifications()
        {
            return View();
        }

    }
}
